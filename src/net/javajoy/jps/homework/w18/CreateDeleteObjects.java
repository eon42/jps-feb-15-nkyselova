package net.javajoy.jps.homework.w18;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class CreateDeleteObjects {
    public static final int SIZE = 4096;
    public static final int ITERATION = 25;
    public static final int DELAY_IN_MILLIS = 200;

    private List<byte[]> bytes = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        CreateDeleteObjects main = new CreateDeleteObjects();
        main.create();
    }

    private void create() throws InterruptedException {
        int i = 0;

        while (true) {
            bytes.add(new byte[SIZE * SIZE]);
            Thread.sleep(DELAY_IN_MILLIS);
            System.out.printf("%d Creating object...%n", i);
            if (i >= 0 && i < ITERATION) {
                i++;
            } else {
                int objectCount = (int) (Math.random() * ITERATION);
                i = i - objectCount;
                delete(objectCount);
            }
        }
    }

    private void delete(int count) throws InterruptedException {
        for (int i = count; i >= 0; i--) {
            if (bytes != null) {
                bytes.remove(i);
            }
            Thread.sleep(DELAY_IN_MILLIS);
            System.out.printf("%d Deleting object...%n", i);
        }
    }
}
