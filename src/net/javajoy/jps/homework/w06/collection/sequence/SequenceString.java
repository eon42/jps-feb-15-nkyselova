package net.javajoy.jps.homework.w06.collection.sequence;

/**
 * @author Kiselova Nataliia
 */

public interface SequenceString extends Iterable {

    /**
     * Search element in sequence by number
     *
     * @param n position element
     * @return element
     */
    String searchByNumber(int n);

    /**
     * Search first element in sequence by value
     *
     * @param elem value element
     * @return position
     */
    int indexOf(String elem);

    /**
     * Add SequenceLines elem in n position
     *
     * @param elem element, that should be added
     * @param n    position, in which elem should be added
     * @return SequenceString
     */
    void insert(String elem, int n);

    /**
     * Delete element in n position
     *
     * @param n position
     * @return SequenceString
     */
    void deleteByNumber(int n);

    /**
     * Delete first element in sequence by value
     *
     * @param elem value element
     * @return SequenceString
     */
    void deleteByValue(String elem);
}
