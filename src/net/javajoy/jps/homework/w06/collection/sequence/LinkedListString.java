package net.javajoy.jps.homework.w06.collection.sequence;

import java.util.Iterator;

/**
 * @author Kiselova Nataliia
 */

public class LinkedListString implements SequenceString, Iterable {
    private int size;

    /**
     * Class Node for linked list
     */
    public static class Node {
        private String value = null;
        public Node next = null;

        /**
         * Constructor
         *
         * @param value String
         * @param next  Node
         */
        Node(String value, Node next) {
            this.value = value;
            this.next = next;
        }

        /**
         * Getter
         *
         * @return String value
         */
        public String getValue() {
            return value;
        }
    }

    public Node head;

    /**
     * Show int size (real number of Strings) in LinkedList
     *
     * @return size of LinkedList
     */
    public int size() {
        return size;
    }

    /**
     * Search element in sequence by number
     *
     * @param n position element
     * @return element
     */
    @Override
    public String searchByNumber(int n) {
        Node node;
        int i = 0;

        for (node = head; node != null && i != n; node = node.next) {
            i++;
        }

        return node == null ? null : node.getValue();
    }

    /**
     * Search first element in sequence by value
     *
     * @param elem value element
     * @return position
     */
    @Override
    public int indexOf(String elem) {
        Node node;
        int i = 0;

        for (node = head; node != null && !node.value.equals(elem); node = node.next) {
            i++;
        }

        return node == null ? -1 : i;
    }

    /**
     * Add SequenceLines elem in n position
     *
     * @param elem element, that should be added
     * @param n    position, in which elem should be added
     */
    @Override
    public void insert(String elem, int n) {
        if (head == null) {
            head = new Node(elem, null);
            size = 1;
            return;
        }

        Node temp;

        if (n == 0) {
            temp = head;
            head = new Node(elem, temp);
            size++;
            return;
        }

        Node node = head;
        int i = 1;

        while (node.next != null) {
            if (i++ == n) {
                temp = node.next;
                node.next = new Node(elem, temp);
                size++;
                return;
            } else {
                node = node.next;
            }
        }

        node.next = new Node(elem, null);
        size++;
    }


    /**
     * Delete element in n position
     *
     * @param n position
     */
    @Override
    public void deleteByNumber(int n) {
        Node node;
        int i = 0;

        for (node = head; node != null && i != n - 1; node = node.next) {
            i++;
        }

        Node temp;
        if (node != null) {
            temp = node.next;
            node.next = temp.next;
            size--;
        }
    }

    /**
     * Delete first element in sequence by value
     *
     * @param elem value element
     */
    @Override
    public void deleteByValue(String elem) {
        Node node;

        for (node = head; node != null && !node.next.value.equals(elem); ) {
            node = node.next;
        }

        Node temp;
        if (node != null) {
            temp = node.next;
            node.next = temp.next;
            size--;
        }
    }

    /**
     * Clear list
     */
    public void clear() {
        head = null;
        size = 0;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator iterator() {
        return new ObjIterator();
    }

    /**
     * Class Iterator for linked list
     */
    public class ObjIterator implements Iterator {
        Node node = head;

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext() {
            return node != null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object next() {
            Node temp;
            if (hasNext()) {
                temp = node;
                node = node.next;
            } else {
                temp = null;
            }
            return temp != null ? temp.getValue() : null;
        }
    }
}
