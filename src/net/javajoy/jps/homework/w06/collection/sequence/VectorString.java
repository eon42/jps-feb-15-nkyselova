package net.javajoy.jps.homework.w06.collection.sequence;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Kiselova Nataliia
 */

public class VectorString implements SequenceString, Iterable {
    private static final int DEFAULT_CAPACITY = 10;

    private String[] data;
    private int capacity;
    private int size;

    /**
     * Constructor
     *
     * @param capacity int
     */
    public VectorString(int capacity) {
        this.capacity = capacity;
        this.data = new String[this.capacity];
    }

    /**
     * Constructor
     */
    public VectorString() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Show int size (real number of Strings) in Vector
     *
     * @return size of Vector
     */
    public int size() {
        return size;
    }

    /**
     * Search element in sequence by number
     *
     * @param n position element
     * @return element
     */
    @Override
    public String searchByNumber(int n) {
        return data[n];
    }

    /**
     * Search first element in sequence by value
     *
     * @param elem value element
     * @return position
     */
    @Override
    public int indexOf(String elem) {
        for (int i = 0; i < size; i++) {
            if (data[i].equals(elem)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Add SequenceLines elem in n position
     *
     * @param elem element, that should be added
     * @param n    position, in which elem should be added
     */
    @Override
    public void insert(String elem, int n) {
        if (size < capacity) {
            System.arraycopy(data, n, data, n + 1, size - n);
            data[n] = elem;
        } else {
            capacity = size + capacity / 2;
            String[] newData = new String[capacity];
            System.arraycopy(data, 0, newData, 0, size);
            data = newData;
            System.arraycopy(data, n, data, n + 1, size - n);
            data[n] = elem;
        }
        size++;
    }

    /**
     * Delete element in n position
     *
     * @param n position
     */
    @Override
    public void deleteByNumber(int n) {
        System.arraycopy(data, n, data, n - 1, size - n);
        data[--size] = null;
    }

    /**
     * Delete all element in sequence by value
     *
     * @param elem value element
     */
    @Override
    public void deleteByValue(String elem) {
        for (int i = 0; i < size; i++) {
            if (data[i].equals(elem)) {
                System.arraycopy(data, i + 1, data, i, size - i--);
                size--;
            }
        }
    }

    /**
     * Check if Vector is empty or not
     *
     * @return true if Vector is empty
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Setter
     *
     * @param index int
     * @param value String
     */
    public void set(int index, String value) {
        if (index > 0 && index < capacity) {
            data[index] = value;
        }
    }

    /**
     * Getter
     *
     * @param index int
     * @return String value by index
     */
    public String get(int index) {
        if (index >= 0 && index < capacity) {
            return data[index];
        } else {
            return null;
        }
    }

    /**
     * ToString
     *
     * @return String
     */
    public String toString() {
        return Arrays.toString(data);
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator iterator() {
        return new ObjIterator();
    }

    /**
     * Class Iterator for vector
     */
    public class ObjIterator implements Iterator {
        private int position;

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext() {
            return position < data.length;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object next() {
            if (hasNext()) {
                return data[position++];
            } else {
                throw new NoSuchElementException(
                        String.format("There is no element on position %d", position));
            }
        }

    }

    public int getCapacity() {
        return capacity;
    }
}
