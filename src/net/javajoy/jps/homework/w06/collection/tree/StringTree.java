package net.javajoy.jps.homework.w06.collection.tree;

/**
 * @author Kiselova Nataliia
 */
public class StringTree { //AN: переименовал, так как казалось что у нас дерево строка, хотя у нас дерево из строк. Так на аглийском более правильно сейчас
    String value;
    private StringTree left;
    private StringTree right;


    public StringTree(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public StringTree getLeft() {
        return left;
    }

    public void setLeft(StringTree left) {
        this.left = left;
    }

    public StringTree getRight() {
        return right;
    }

    public void setRight(StringTree right) {
        this.right = right;
    }

    public void add(String value) {
        if (value.compareTo(this.value) == -1) {
            left.add(value);
        } else {
            right.add(value);
        }
    }
}
