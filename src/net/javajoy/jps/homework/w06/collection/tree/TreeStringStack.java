package net.javajoy.jps.homework.w06.collection.tree;

import net.javajoy.jps.homework.w06.collection.stack.Stack;

/**
 * @author Kiselova Nataliia
 */
public class TreeStringStack {

    private Stack<StringTree> data;

    public TreeStringStack() {
        this.data = new Stack<>();
    }

    public void push(StringTree element) throws Exception {
        data.push(element);
    }

    public StringTree pop() {
        return data.pop();
    }

    public StringTree peek() {
        return data.peek();
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }
}
