package net.javajoy.jps.homework.w06.collection.stack;

import net.javajoy.jps.homework.w06.collection.sequence.VectorString;

/**
 * @author Kiselova Nataliia
 */
public class StackString {

    private VectorString stack;

    /**
     * {@inheritDoc}
     */
    public StackString(int capacity) {
        stack = new VectorString(capacity);
    }

    /**
     * {@inheritDoc}
     */
    public StackString() {
        stack = new VectorString();
    }

    /**
     * Push element to the top of the Stack
     *
     * @param element
     * @throws Exception
     */
    public void push(String element) throws Exception {
        if (stack.size() < stack.getCapacity()) {
            stack.insert(element, 0);
        } else {
            throw new Exception("Stack overflow");
        }
    }

    /**
     * Return element on the top of the Stack. After this method called on the top will be next element.
     *
     * @return element on the top of the Stack.
     */
    public String pop() {
        if (stack.size() > 0) {
            String s = stack.get(0);
            stack.deleteByNumber(1);
            return s;
        } else {
            return null;
        }
    }

    /**
     * Return element on the top of the Stack. This element remains at the top.
     *
     * @return element on the top of the Stack.
     */
    public String peek() {
        return (stack.size() > 0) ? stack.get(0) : null;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEmpty() {
        return stack.size() == 0;
    }
}
