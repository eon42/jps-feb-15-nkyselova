package net.javajoy.jps.homework.w06.collection.stack;

/**
 * @author Kiselova Nataliia
 */
public class Stack<T> {
    public static final int MAX_CAPACITY = 10;

    private Object[] array;
    private int top = -1;

    /**
     * Constructor
     */
    public Stack() {
        array = new Object[MAX_CAPACITY];
    }

    /**
     * Pop top element from stack
     *
     * @return null if stack is empty
     */
    public T pop() {
        if (top > -1) {
            return (T)array[top--];
        } else {
            return null;
        }
    }

    /**
     * Push to stack one more element
     *
     * @param element
     * @throws Exception if stack is full
     */
    public void push(T element) throws Exception {
        if (top < array.length) {
            array[++top] = element;
        } else {
            throw new Exception("Stack overflow");
        }
    }

    /**
     * Show top element
     *
     * @return top element
     */
    public T peek() {
        return (top > -1) ? (T)array[top] : null;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <= top; i++) {
            if (array[i] != null) {
                builder.append(array[i]).append(", ");
            }
        }

        return builder.toString();
    }

    /**
     * Check weather stack is empty
     *
     * @return true if stack is empty
     */
    public boolean isEmpty() {
        return top < 0;
    }
}
