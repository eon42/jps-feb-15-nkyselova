package net.javajoy.jps.homework.w06;

import net.javajoy.jps.homework.w06.collection.sequence.LinkedListString;
import net.javajoy.jps.homework.w06.collection.sequence.SequenceString;
import net.javajoy.jps.homework.w06.collection.sequence.VectorString;
import net.javajoy.jps.homework.w06.collection.stack.Stack;
import net.javajoy.jps.homework.w06.collection.stack.StackString;
import net.javajoy.jps.homework.w06.collection.tree.StringTree;
import net.javajoy.jps.homework.w06.collection.tree.TreeStringStack;
import net.javajoy.jps.homework.w06.util.StringUtils;

import java.util.Iterator;

/**
 * @author Kiselova Nataliia
 */
public class Demo {

    public static void main(String[] args) throws Exception {
        testVector();
        testLinkedList();
        testStack("}if(a>b()) f(s){;");
        testStack("if(a>b()) f(s);");
        testStackString();//Third point task
        testTreeString();//Fourth point task
    }

    private static void testVector() {
        SequenceString vector = new VectorString(5);

        vector.insert("1", 0);
        vector.insert("2", 1);
        vector.insert("6", 2);
        vector.insert("5", 2);
        vector.insert("5", 2);
        vector.insert("Vector", 2);

        assert ((VectorString) vector).size() == 6;
        //AN: запустить программу с assert можно добавив в Run Configuration: VM option = -ea

        Iterator it = vector.iterator();

        System.out.println("\nView Vector using iterator");
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        int i = 2;
        vector.deleteByNumber(i);
        System.out.println("\nAfter delete " + i + " element " + vector);

        assert ((VectorString) vector).size() == 5;

        String s5 = "5";
        vector.deleteByValue(s5);
        System.out.println("After delete by value " + s5 + ", is " + vector);
        assert ((VectorString) vector).size() == 3;

        System.out.println(i + " element = " + vector.searchByNumber(2));

        String searchString = "Vector";
        System.out.printf("Number of element, which is equal \"%s\", is %d%n", searchString, vector.indexOf(searchString));
        System.out.println("******************************************************************************");
    }

    private static void testLinkedList() {
        SequenceString list = new LinkedListString();
        list.insert("10", 0);
        list.insert("20", 1);
        list.insert("30", 0);
        list.insert("40", 1);
        list.insert("50", 3);
        list.insert("60", 3);

        assert ((LinkedListString) list).size() == 6;

        System.out.println("\nView linked list using iterator");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        System.out.println("\nLinked list: deleted third element (counting from 0)");
        list.deleteByNumber(3);
        it = list.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        assert ((LinkedListString) list).size() == 5;

        System.out.println("\nLinked list: deleted element, which has value \"40\"");
        list.deleteByValue("40");
        it = list.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        assert ((LinkedListString) list).size() == 4;
        System.out.println("\nLinked list: search element, which has value \"10\": " + list.indexOf("10"));

        System.out.printf("%nLinked list: search element, which has position 3: %s%n%n", list.searchByNumber(3));
        System.out.println("******************************************************************************");
    }

    private static void testStack(String input) throws Exception {
        System.out.println("String, which is check:" + input);
        Stack<String> stack = new Stack<>();

        for (char c : input.toCharArray()) {
            if (StringUtils.isBracket(c)) {
                if (StringUtils.isPairBracket(String.valueOf(c), String.valueOf(stack.peek()))) {
                    stack.pop();
                } else {
                    stack.push(String.valueOf(c));
                }
            }
        }

        System.out.printf("Stack content is %s, result: %s%n", stack, stack.isEmpty() ? "OK" : "WRONG");
    }

    private static void testStackString() throws Exception {
        StackString stack = new StackString();
        stack.push("One");
        stack.push("Two");
        stack.push("Three");
        stack.push("Four");

        String top;
        System.out.println("Contents stack:");
        while ((top = stack.pop()) != null) {
            System.out.println(top);
        }

        stack.push("One");
        stack.push("Two");
        stack.push("Three");
        stack.push("Four");

        String str = "Four";
        if (stack.peek().equals(str)) {
            stack.pop();
        }

        System.out.printf("\nContents stack after pop %s:\n", str);
        while ((top = stack.pop()) != null) {
            System.out.println(top);
        }

        System.out.println("******************************************************************************");
    }

    private static void testTreeString() throws Exception {
        StringTree root = new StringTree("6");

        StringTree left = new StringTree("4");
        left.setLeft(new StringTree("3"));
        left.setRight(new StringTree("5"));
        root.setLeft(left);

        StringTree right = new StringTree("8");
        right.setLeft(new StringTree("7"));
        right.setRight(new StringTree("10"));
        root.setRight(right);

        System.out.println("\nDFS");//нерекурсивный поиск в глубину (вертикальный прямой обход)
        dfsByStack(root);

    }

    private static void dfsByStack(StringTree stringTree) throws Exception {
        TreeStringStack stack = new TreeStringStack();
        stack.push(stringTree);

        while (stringTree != null || !stack.isEmpty()) {
            if (!stack.isEmpty()) {
                stringTree = stack.pop();
                System.out.print(stringTree.getValue() + " ");
            }
            while (stringTree != null) {
                if (stringTree.getRight() != null) {
                    stack.push(stringTree.getRight());
                }
                stringTree = stringTree.getLeft();
                if (stringTree != null) {
                    System.out.print(stringTree.getValue() + " ");
                }
            }
        }
    }
}
