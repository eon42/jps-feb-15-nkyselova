package net.javajoy.jps.homework.w06.util;

/**
 * @author Kiselova Nataliia
 */
public class StringUtils {
    public static final String KEY = "{}()";

    /**
     * Check if char is bracket
     *
     * @param c char
     * @return true if char is bracket
     */
    public static boolean isBracket(char c) {
        return KEY.contains(String.valueOf(c));
    }

    /**
     *Check if element in string is pair for bracket
     *
     * @param element String
     * @param stackElement String
     * @return true if element in string is pair for bracket
     */
    public static boolean isPairBracket(String element, String stackElement) {
        if (stackElement != null) {
            switch (element) {
                case ")":
                    return stackElement.equals("(");
                case "}":
                    return stackElement.equals("{");
            }
        }

        return false;
    }
}
