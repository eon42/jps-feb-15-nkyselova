package net.javajoy.jps.homework.w04;

import net.javajoy.jps.homework.w04.matrix.ImmutableMatrix;
import net.javajoy.jps.homework.w04.matrix.MutableMatrix;

public class Main {

    public static void main(String[] args) {
        demoMatrix();
        demoImmutableMatrix();
    }

    private static void demoMatrix() {
        System.out.println("First instance of MutableMatrix class");
        MutableMatrix firstMatrix = new MutableMatrix(createMatrix(1, 2));
        MutableMatrix.printMatrix(firstMatrix);

        System.out.println("Second instance of MutableMatrix class");
        MutableMatrix secondMatrix = new MutableMatrix(createMatrix(2, 3));
        MutableMatrix.printMatrix(secondMatrix);

        System.out.println("Third instance of MutableMatrix class");
        MutableMatrix thirdMatrix = new MutableMatrix(createMatrix(2, 3));
        MutableMatrix.printMatrix(thirdMatrix);

        System.out.println("Matrix - result of addition second and third instances of MutableMatrix class:");
        MutableMatrix resultAddMatrix = (MutableMatrix) secondMatrix.add(thirdMatrix);
        MutableMatrix.printMatrix(resultAddMatrix);

        System.out.println("Changed second instance (if addition was successful):");
        MutableMatrix.printMatrix(secondMatrix);

        System.out.println("Matrix - result of multiplication first and third instances of MutableMatrix class:");
        MutableMatrix resultMultiplyMatrix = (MutableMatrix) firstMatrix.multiply(thirdMatrix);
        MutableMatrix.printMatrix(resultMultiplyMatrix);

        System.out.println("Changed first instance (if multiplication was successful):");
        MutableMatrix.printMatrix(firstMatrix);

        System.out.println("*********************************************");
    }

    private static void demoImmutableMatrix() {
        System.out.println("First instance of ImmutableMatrix class");
        ImmutableMatrix firstImMatrix = new ImmutableMatrix(createMatrix(1, 2));
        ImmutableMatrix.printMatrix(firstImMatrix);

        System.out.println("Second instance of ImmutableMatrix class");
        ImmutableMatrix secondImMatrix = new ImmutableMatrix(createMatrix(2, 3));
        ImmutableMatrix.printMatrix(secondImMatrix);

        System.out.println("Third instance of ImmutableMatrix class");
        ImmutableMatrix thirdImMatrix = new ImmutableMatrix(createMatrix(2, 3));
        ImmutableMatrix.printMatrix(thirdImMatrix);

        System.out.println("Matrix - result of addition second and third instances of ImmutableMatrix class:");
        ImmutableMatrix resultAddImMatrix = (ImmutableMatrix) secondImMatrix.add(thirdImMatrix);
        ImmutableMatrix.printMatrix(resultAddImMatrix);

        System.out.println("Not changed second instance:");
        ImmutableMatrix.printMatrix(secondImMatrix);

        System.out.println("Matrix - result of multiplication first and third instances of ImmutableMatrix class:");
        ImmutableMatrix resultMultiplyImMatrix = (ImmutableMatrix) firstImMatrix.multiply(thirdImMatrix);
        ImmutableMatrix.printMatrix(resultMultiplyImMatrix);

        System.out.println("Not changed first instance:");
        ImmutableMatrix.printMatrix(firstImMatrix);
    }

    private static int[][] createMatrix(int n, int m) {
        int[][] resultMatrix = new int[m][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                resultMatrix[j][i] = (int) (Math.random() * 10);
            }
        }
        return resultMatrix;
    }
}
