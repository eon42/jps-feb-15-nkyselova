package net.javajoy.jps.homework.w04.matrix;

/**
 * Immutable class, which extends class MutableMatrix
 *
 * @author Kiselova Nataliia
 */
public final class ImmutableMatrix extends MutableMatrix {

    /**
     * {@inheritDoc}
     */
    public ImmutableMatrix(int[][] matrix) {
        super(matrix);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Operable add(Operable operand) {
        if (operand == null) { //AN: здесь лишняя проверка на null.
            // Мы можем объявить требования к методу, чтобы операнд был не null,
            // если пользователь туда поставит null, то сам виноват, пусть исправляет ошибку в своей коде
            // KN: если в demoImmutableMatrix сделать ImmutableMatrix thirdImMatrix = null,
            // KN: то при вызове метода secondImMatrix.add(thirdImMatrix) будет NullPointerException.
            // KN: Тогда нужно вернуть проверку на null перед вызовом метода, а мы как раз от этого хотели избавиться
            // AN: все верно и это нормально. Мы делаем контракт к методу, то есть пользователь должен передавать не null
            // AN: если он все-таки передал null, то значит будет RuntimeException здесь. Тогда ему нужно найти проблему в своем коде и понять почему передался null
            // AN: здесь философский момент.
            return null;
        }
        return new ImmutableMatrix(sumInstances((int[][]) this.getData(), (int[][]) operand.getData()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Operable multiply(Operable operand) {
        if (operand == null) { //AN: здесь лишняя проверка на null.
            // Мы можем объявить требования к методу, чтобы операнд был не null,
            // если пользователь туда поставит null, то сам виноват, пусть исправляет ошибку в своей коде
            return null;
        }
        return new ImmutableMatrix(multiplyInstances((int[][]) this.getData(), (int[][]) operand.getData()));
    }
}

