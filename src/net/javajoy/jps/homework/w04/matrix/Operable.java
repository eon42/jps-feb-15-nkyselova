package net.javajoy.jps.homework.w04.matrix;

/**
 * Interface Operable deal wits two-dimensional arrays with methods: add and multiply two matrix.
 * Parameters and result of this two methods is Operable type.
 *
 * @author Kiselova Nataliia
 */
public interface Operable {
    /**
     * Add two objects
     *
     * @param matrix is object Operable
     * @return Operable object
     */
    Operable add(Operable matrix);

    /**
     * Multiple two objects
     *
     * @param matrix is object Operable
     * @return Operable object
     */
    Operable multiply(Operable matrix);

    /**
     * Check whether matrix is an instance of class MutableMatrix
     *
     * @param matrix, which is checked
     * @return boolean true, if matrix is an instance of class MutableMatrix
     */
    static boolean isValid(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0) {
            return false;
        } else {
            int length = matrix[0].length;
            for (int i = 1; i < matrix.length; i++) {
                if (!(matrix[i].length == length)) return false;
            }
        }
        return true;
    }

    /**
     * Get data (getter)
     *
     * @return Object object
     */
    Object getData();
}
