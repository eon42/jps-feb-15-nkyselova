package net.javajoy.jps.homework.w04.matrix;

/**
 * Mutable class, which implements interface Operable
 *
 * @author Kiselova Nataliia
 */
public class MutableMatrix implements Operable {
    private int[][] data;

    /**
     * Empty constructor
     */
    public MutableMatrix() {

    }

    /**
     * Constructor
     *
     * @param matrix data for constructor
     */
    public MutableMatrix(int[][] matrix) {
        this.data = matrix;
    }

    /**
     * Constructor, create matrix n*m, using random number
     *
     * @param row    amount of line in matrix
     * @param column amount of column in matrix
     */
    public MutableMatrix(int row, int column) {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                this.data[j][i] = (int) (Math.random() * 10);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Operable add(Operable operand) {
        if (operand == null) {
            return null;
        }
        int[][] operandData = (int[][]) operand.getData();
        this.data = sumInstances(this.data, operandData);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Operable multiply(Operable operand) {
        if (operand == null) {
            return null;
        }
        int[][] operandData = (int[][]) operand.getData();
        this.data = multiplyInstances(this.data, operandData);
        return this;
    }

    /**
     * Print matrix to console
     */
    public static void printMatrix(MutableMatrix matrix) {
        if (matrix != null) {
            for (int i = 0; i < matrix.data[0].length; i++) {
                for (int[] aData : matrix.data) {
                    System.out.print(aData[i] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    /**
     * Sum up two matrices
     *
     * @param matrix1 first operand in addition
     * @param matrix2 second operand in addition
     * @return matrix, the result of addition of two matrices, or null,
     * if define incorrectly or matrices can't be added
     */
    protected int[][] sumInstances(int[][] matrix1, int[][] matrix2) {
        int firstDimension = matrix1.length;
        int secondDimension = matrix1[0].length;
        int[][] resultMatrix = new int[firstDimension][secondDimension];

        if (!(Operable.isValid((matrix1)) && Operable.isValid((matrix2))
                && this.canBeAdded(matrix2))) {
            return null;
        }

        for (int i = 0; i < firstDimension; i++) {
            for (int j = 0; j < secondDimension; j++) {
                resultMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return resultMatrix;
    }

    /**
     * Multiply two matrices
     *
     * @param matrix1 first operand in multiplication
     * @param matrix2 second operand in multiplication
     * @return matrix, the result of multiplication of two matrices, or null,
     * if define incorrectly or matrices can't be multiplied
     */
    protected int[][] multiplyInstances(int[][] matrix1, int[][] matrix2) {
        int sum = 0;
        int firstDimension = matrix1[0].length;
        int secondDimension = matrix2.length;
        int thirdDimension = matrix2[0].length;
        int resultMatrix[][] = new int[secondDimension][firstDimension];

        if (!(Operable.isValid((matrix1)) && Operable.isValid((matrix2))
                && this.canBeMultiplied(matrix2))) {
            return null;
        }

        for (int i = 0; i < firstDimension; i++) {
            for (int j = 0; j < secondDimension; j++) {
                for (int k = 0; k < thirdDimension; k++) {
                    sum += matrix1[k][i] * matrix2[j][k];
                }
                resultMatrix[j][i] = sum;
                sum = 0;
            }
        }
        return resultMatrix;
    }

    /**
     * Check whether MutableMatrix matrix and this object can be added
     *
     * @return boolean true, if matrices matrix and this object can be added
     */
    public boolean canBeAdded(int[][] matrix) {
        return this.data.length == matrix.length && this.data[0].length == matrix[0].length;
    }

    /**
     * Check whether MutableMatrix matrix and this object can be multiplied
     *
     * @return boolean true, if matrices matrix and matrix can be multiplied
     */
    public boolean canBeMultiplied(int[][] matrix) {
        return this.data.length == matrix[0].length;
    }

    /**
     * Setter
     *
     * @param matrix data
     */
    protected void copyArray(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.arraycopy(matrix[i], 0, this.data[i], 0, matrix[i].length);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getData() {
        return this.data;
    }
}
