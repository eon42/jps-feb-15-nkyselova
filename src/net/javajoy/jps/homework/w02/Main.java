package net.javajoy.jps.homework.w02;

/*
  2. Создать два объекта «время» заполнив их значениями из консоли, используя разные способы создания экземпляра объекта.
  Вывести результаты сравнения двух объектов на консоль (кто больше из них, разница в секундах).
  Подумайте, как бы вы могли обезопасить свой класс от ввода некорректных данных.
  Если такое происходит, следует информировать об этом пользователя.
*/

import java.util.Scanner;

import static net.javajoy.jps.homework.w02.Time.compare;

public class Main {
    public static final int MAX_HOUR = 23;
    public static final int MAX_MIN = 59;
    public static final int MAX_SEC = 59;
    public static final int MIN_ANY_TIME = 0;

    public static void main(String[] args) {
        Time[] times = dataInput();
        printResult(times);
    }

    /**
     * Input data
     */
    private static Time[] dataInput() {
        Time[] times = new Time[2];
        int[] arrayTime;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое значение времени в формате чч:мм:сс");

        while (scanner.hasNext()) {
            try {
                arrayTime = readTime(scanner);
                times[0] = new Time(arrayTime[0], arrayTime[1], arrayTime[2]);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Вы ввели неправильное значение времени, нужно вводить в формате чч:мм:сс. " +
                        "Повторите ввод");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("Введите второе значение времени в формате чч:мм:сс");

        while (scanner.hasNext()) {
            try {
                arrayTime = readTime(scanner);
                times[1] = new Time();
                times[1].setHour(arrayTime[0]);
                times[1].setMin(arrayTime[1]);
                times[1].setSec(arrayTime[2]);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Вы ввели неправильное значение времени, нужно вводить в формате чч:мм:сс. " +
                        "Повторите ввод");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return times;
    }

    /**
     * Read values of Time from console
     *
     * @param scanner
     * @return int[hour, min, sec]
     * @throws Exception
     */
    private static int[] readTime(Scanner scanner) throws Exception {
        int[] arrayTime = new int[3];
        String line = scanner.nextLine();
        String[] array = line.split(":");
        arrayTime[0] = Integer.parseInt(array[0]);//hour
        arrayTime[1] = Integer.parseInt(array[1]);//min
        arrayTime[2] = Integer.parseInt(array[2]);//sec
        validateInputTime(arrayTime[0], arrayTime[1], arrayTime[2]);
        return arrayTime;
    }

    /**
     * Check if value of time satisfy the conditions
     *
     * @param hour
     * @param min
     * @param sec
     * @throws Exception
     */
    private static void validateInputTime(int hour, int min, int sec) throws Exception {
        if (hour > MAX_HOUR || hour < MIN_ANY_TIME || min > MAX_MIN || min < MIN_ANY_TIME ||
                sec > MAX_SEC || sec < MIN_ANY_TIME) {
            throw new Exception("Может быть от 0 до 23 часов, от 0 до 59 минут и секунд. Повторите ввод");
        }
    }

    /**
     * Output results of comparison
     *
     * @param timeFirst
     * @param timeSecond
     */
    private static void printResult(Time timeFirst, Time timeSecond) {
        int diff = timeFirst.countDifferenceInSeconds(timeSecond);
        if (compare(timeFirst, timeSecond)) {
            printFormattedString(timeFirst, timeSecond, diff);
        } else {
            printFormattedString(timeSecond, timeFirst, -diff);
        }
    }

    private static void printResult(Time[] times) {
        printResult(times[0], times[1]);
    }

    /**
     * Print formatted string
     *
     * @param timeFirst
     * @param timeSecond
     * @param diff
     */
    private static void printFormattedString(Time timeFirst, Time timeSecond, int diff) {
        System.out.printf("%02d:%02d:%02d больше, чем %02d:%02d:%02d, на %d сек",
                timeFirst.getHour(), timeFirst.getMin(), timeFirst.getSec(),
                timeSecond.getHour(), timeSecond.getMin(), timeSecond.getSec(), diff);
    }
}
