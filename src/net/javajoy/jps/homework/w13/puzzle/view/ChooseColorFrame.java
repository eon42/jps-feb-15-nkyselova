package net.javajoy.jps.homework.w13.puzzle.view;

import net.javajoy.jps.homework.w13.puzzle.model.ColorNames;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class ChooseColorFrame extends JFrame {
    public static final int WIDTH = 220;
    public static final int HEIGHT = 130;

    private JButton okButton;
    private JFrame frameColor;
    private JRadioButton firstColor;
    private JRadioButton secondColor;
    private JRadioButton thirdColor;
    private JRadioButton forthColor;

    public ChooseColorFrame(Color first, Color second, Color third, Color forth) {
        frameColor = new JFrame();
        frameColor.setLayout(new GridBagLayout());
        ButtonGroup bG = new ButtonGroup();
        ColorNames colorMap = new ColorNames();
        secondColor = createRadioButton(frameColor, bG, 0, 1, 1, colorMap.getTextColorName(second));
        thirdColor = createRadioButton(frameColor, bG, 1, 0, 1, colorMap.getTextColorName(third));
        firstColor = createRadioButton(frameColor, bG, 0, 0, 1, colorMap.getTextColorName(first));
        forthColor = createRadioButton(frameColor, bG, 1, 1, 1, colorMap.getTextColorName(forth));
        forthColor.setSelected(true);
        okButton = initOkButton(frameColor);

        frameColor.setMinimumSize(new Dimension(WIDTH, HEIGHT));
        frameColor.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        frameColor.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frameColor.setLocationRelativeTo(null);
        frameColor.setResizable(false);
        frameColor.setVisible(true);
    }

    private JRadioButton createRadioButton(JFrame frameMarqueeColor, ButtonGroup bG, int gridX, int gridY,
                                           int weightX, String colorName) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = gridX;
        gbc.gridy = gridY;
        gbc.weightx = weightX;
        gbc.anchor = GridBagConstraints.WEST;
        JRadioButton colorButton = new JRadioButton(colorName);
        bG.add(colorButton);
        frameMarqueeColor.add(colorButton, gbc);
        return colorButton;
    }

    private JButton initOkButton(JFrame frameBackgroundColor) {
        GridBagConstraints gbcOk = new GridBagConstraints();
        gbcOk.weightx = 4;
        gbcOk.gridwidth = 2;
        gbcOk.gridy = 2;
        gbcOk.anchor = GridBagConstraints.SOUTH;
        gbcOk.fill = GridBagConstraints.HORIZONTAL;
        gbcOk.insets = new Insets(5, 5, 5, 5);
        JButton okButton = new JButton("Ok");
        frameBackgroundColor.add(okButton, gbcOk);
        return okButton;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JFrame getFrameColor() {
        return frameColor;
    }

    public JRadioButton getFirstColor() {
        return firstColor;
    }

    public JRadioButton getSecondColor() {
        return secondColor;
    }

    public JRadioButton getThirdColor() {
        return thirdColor;
    }

    public JRadioButton getForthColor() {
        return forthColor;
    }
}
