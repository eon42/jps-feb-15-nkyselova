package net.javajoy.jps.homework.w13.puzzle.view;

import net.javajoy.jps.homework.w13.puzzle.Demo13Puzzle;
import net.javajoy.jps.homework.w13.puzzle.controller.GameMouseListener;
import net.javajoy.jps.homework.w13.puzzle.controller.KeyboardListener;
import net.javajoy.jps.homework.w13.puzzle.model.Matrix;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kiselova Nataliia
 */
public class MainFrame extends JFrame {
    public static final int PADDING_IN_PIXELS = 1;

    private final int fieldSize;

    private ToolBarPanel toolBarPanel;
    private Panel panel;
    private JPanel mainPanel;

    public MainFrame(Matrix matrix, KeyboardListener keyboardListener, GameMouseListener gameMouseListener) {
        this.fieldSize = Demo13Puzzle.FIELD_SIZE_IN_PIXEL;
        initFrame(gameMouseListener, matrix);
        keyboardListener.initKeyboardListenerController(this, matrix);
    }

    private void initFrame(GameMouseListener gameMouseListener, Matrix matrix) {
        mainPanel = new JPanel();
        initMainPanel(matrix);
        panel = new Panel(matrix, gameMouseListener);
        toolBarPanel = new ToolBarPanel(matrix);
        setPreferredSize(
                new Dimension(
                        (matrix.getHeight() + PADDING_IN_PIXELS) * fieldSize,
                        (matrix.getWidth() + PADDING_IN_PIXELS) * fieldSize
                )
        );
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Puzzle");
        setResizable(false);
        mainPanel.add(toolBarPanel);
        mainPanel.add(panel);
        setContentPane(mainPanel);
        setJMenuBar(new GameMenu().initGameMainMenu(this, panel, matrix, gameMouseListener));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initMainPanel(Matrix matrix) {
        mainPanel.setPreferredSize(
                new Dimension(
                        (matrix.getHeight() + PADDING_IN_PIXELS) * fieldSize,
                        (matrix.getWidth() + PADDING_IN_PIXELS) * fieldSize
                )
        );
        mainPanel.setMinimumSize(
                new Dimension(
                        (matrix.getHeight() + PADDING_IN_PIXELS) * fieldSize,
                        (matrix.getWidth() + PADDING_IN_PIXELS) * fieldSize
                )
        );
    }

    public ToolBarPanel getToolBarPanel() {
        return toolBarPanel;
    }

    public Panel getPanel() {
        return panel;
    }

    public void setPanel(Panel panel) {
        this.panel = panel;
    }
}
