package net.javajoy.jps.homework.w13.puzzle.view;

import net.javajoy.jps.homework.w13.puzzle.model.Matrix;

import javax.swing.*;
import java.awt.*;

import static net.javajoy.jps.homework.w13.puzzle.Demo13Puzzle.FIELD_SIZE_IN_PIXEL;

/**
 * @author Kyselova Nataliia
 */
public class ToolBarPanel extends JPanel {
    public static final int HEIGHT = 25;
    public static final int PADDING_IN_PIXELS = 2;

    public ToolBarPanel(Matrix matrix) {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension((matrix.getWidth() + PADDING_IN_PIXELS) * FIELD_SIZE_IN_PIXEL, HEIGHT));
        setMinimumSize(new Dimension((matrix.getWidth() + PADDING_IN_PIXELS) * FIELD_SIZE_IN_PIXEL, HEIGHT));
        setBackground(Color.WHITE);
    }
}
