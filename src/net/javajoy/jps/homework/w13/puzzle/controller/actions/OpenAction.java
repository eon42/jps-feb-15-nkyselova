package net.javajoy.jps.homework.w13.puzzle.controller.actions;

import net.javajoy.jps.homework.w13.puzzle.Demo13Puzzle;
import net.javajoy.jps.homework.w13.puzzle.model.Matrix;
import net.javajoy.jps.homework.w13.puzzle.view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author Kyselova Nataliia
 */
public class OpenAction extends AbstractAction {

    public static final int WIDTH = Demo13Puzzle.GRID_DIMENSION_HORIZONTAL;
    public static final int HEIGHT = Demo13Puzzle.GRID_DIMENSION_VERTICAL;

    private Panel panel;
    private Color chosenBoarder;

    public OpenAction(Panel panel) {
        this.panel = panel;
        putValue(Action.NAME, "Open");
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        File file = new File(SaveAction.SAVE_PUZZLE_TXT);

        if (!file.exists()) {
            JOptionPane.showMessageDialog(panel, String.format("File %sto load data from does not exist", file));
            return;
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("savePuzzle.txt"))) {
            loadData(ois, Matrix.getInstance(WIDTH, HEIGHT));
            JOptionPane.showMessageDialog(panel, "Data have been loaded");
            panel.repaintPanel(chosenBoarder);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void loadData(ObjectInputStream ois, Matrix matrix) {
        try {
            int width = ois.readByte();
            int height = ois.readByte();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    matrix.setElement(i, j, ois.readInt());
                }
            }
            chosenBoarder = new Color(ois.readInt());
            panel.setChosenBoarder(chosenBoarder);
            panel.setBackground(new Color(ois.readInt()));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
