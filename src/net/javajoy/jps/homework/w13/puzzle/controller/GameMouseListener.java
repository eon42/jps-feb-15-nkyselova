package net.javajoy.jps.homework.w13.puzzle.controller;

import net.javajoy.jps.homework.w13.puzzle.model.Matrix;
import net.javajoy.jps.homework.w13.puzzle.view.Panel;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Kiselova Nataliia
 */
public class GameMouseListener extends MouseAdapter {
    private Matrix matrix;
    private Panel panel;

    public void initMouseListenerController(Panel panel, Matrix matrix, JLabel label, int x, int y) {
        this.matrix = matrix;
        this.panel = panel;

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                matrix.renewCurrent(x, y);
                matrix.doOnClick();
                matrix.setNothingWasPressed(true);
                matrix.renewCurrent(Matrix.UNDEFINED_VALUE, Matrix.UNDEFINED_VALUE);
                exitIfGameWon();
            }
        };

        label.addMouseListener(mouseAdapter);
    }

    private void exitIfGameWon() {
        if (matrix.isGameOver()) {
            JOptionPane.showMessageDialog(panel, "Congratulations! You won!");
            System.exit(0);
        }
    }
}

