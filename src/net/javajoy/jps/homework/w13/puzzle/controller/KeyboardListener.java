package net.javajoy.jps.homework.w13.puzzle.controller;

import net.javajoy.jps.homework.w13.puzzle.model.Matrix;
import net.javajoy.jps.homework.w13.puzzle.view.MainFrame;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Kiselova Nataliia
 */
public class KeyboardListener extends KeyAdapter {
    private Matrix matrix;
    private MainFrame frame;

    public void initKeyboardListenerController(MainFrame frame, Matrix matrix) {
        this.matrix = matrix;
        this.frame = frame;
        frame.addKeyListener(createKeyListener());
    }

    private KeyAdapter createKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_SPACE:
                        doOnSpace();
                        break;
                    case KeyEvent.VK_RIGHT:
                        doOnRight();
                        break;
                    case KeyEvent.VK_LEFT:
                        doOnLeft();
                        break;
                    case KeyEvent.VK_UP:
                        doOnUp();
                        break;
                    case KeyEvent.VK_DOWN:
                        doOnDown();
                        break;
                    default:
                        break;
                }
            }
        };
    }

    private void doOnDown() {
        setCurrentCell(matrix.getCurrentX() + 1 >= matrix.getWidth()
                        ? matrix.getCurrentX()
                        : matrix.getCurrentX() + 1,
                matrix.getCurrentY());
    }

    private void doOnUp() {
        setCurrentCell(matrix.getCurrentX() - 1 < 0
                        ? matrix.getCurrentX()
                        : matrix.getCurrentX() - 1,
                matrix.getCurrentY());
    }

    private void doOnLeft() {
        setCurrentCell(matrix.getCurrentX(),
                matrix.getCurrentY() - 1 < 0
                        ? matrix.getCurrentY()
                        : matrix.getCurrentY() - 1);
    }

    private void doOnRight() {
        setCurrentCell(matrix.getCurrentX(),
                matrix.getCurrentY() + 1 >= matrix.getHeight()
                        ? matrix.getCurrentY()
                        : matrix.getCurrentY() + 1);
    }

    private void doOnSpace() {
        matrix.doOnClick();
    }

    private void setCurrentCell(int x, int y) {
        if (matrix.isNothingWasPressed()) {
            matrix.setFirstCell();
        } else {
            matrix.setPreCurrentX(matrix.getCurrentX());
            matrix.setPreCurrentY(matrix.getCurrentY());
            matrix.setCurrentX(x);
            matrix.setCurrentY(y);
        }
        exitIfGameWon();
    }

    private void exitIfGameWon() {
        if (matrix.isGameOver()) {
            JOptionPane.showMessageDialog(frame.getRootPane(), "Congratulations! You won!");
            System.exit(0);
        }
    }
}
