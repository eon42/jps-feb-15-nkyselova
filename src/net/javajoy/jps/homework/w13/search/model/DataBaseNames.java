package net.javajoy.jps.homework.w13.search.model;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author Kyselova Nataliia
 */
public class DataBaseNames {

    // AS: желательно конечно через геттер доступ осуществлять
    // но если кроме тебя никто не будет пользоваться, то ок
    private Set<String> storage = new TreeSet<>();

    /**
     * Fill base
     *
     * @return filled base
     */
    public DataBaseNames fillData() {
        storage.add("Anna");
        storage.add("Andrew");
        storage.add("Angelina");
        storage.add("Angelika");
        storage.add("Anton");
        storage.add("Alexey");
        storage.add("Artem");
        storage.add("Alexander");
        return this;
    }

    public Set<String> getStorage() {
        return storage;
    }
}
