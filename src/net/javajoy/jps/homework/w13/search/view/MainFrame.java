package net.javajoy.jps.homework.w13.search.view;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class MainFrame extends JFrame {

    private JTextField textFieldName;
    private JLabel labelTips;

    public MainFrame() {
        setPreferredSize(new Dimension(200, 120));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        initFrame();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initFrame() {
        textFieldName = new JTextField("");
        labelTips = new JLabel(" ");
        setLayout(new GridBagLayout());
        getContentPane().add(textFieldName, createCellConstraintsTextField());
        getContentPane().add(labelTips, createCellConstraintsLabel());
    }

    private GridBagConstraints createCellConstraintsLabel() {
        return new GridBagConstraints(
                0,
                1,
                1,
                1,
                1,
                0,
                GridBagConstraints.NORTHWEST,
                GridBagConstraints.BOTH,
                new Insets(5, 5, 5, 5),
                0,
                0
        );
    }

    private GridBagConstraints createCellConstraintsTextField() {
        return new GridBagConstraints(
                0,
                0,
                1,
                1,
                1,
                0,
                GridBagConstraints.SOUTHWEST,
                GridBagConstraints.BOTH,
                new Insets(5, 5, 5, 5),
                0,
                0
        );
    }

    public JTextField getTextFieldName() {
        return textFieldName;
    }

    public JLabel getLabelTips() {
        return labelTips;
    }
}
