package net.javajoy.jps.homework.w13.search;

import net.javajoy.jps.homework.w13.search.controller.Controller;
import net.javajoy.jps.homework.w13.search.model.DataBaseNames;
import net.javajoy.jps.homework.w13.search.view.MainFrame;

/**
 * @author Kiselova Nataliia
 */
public class Demo13SearchName {

    public static void main(String[] args) {
        new Controller(new MainFrame(), new DataBaseNames().fillData());
    }

}
