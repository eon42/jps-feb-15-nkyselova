package net.javajoy.jps.homework.w13.search.controller;

import net.javajoy.jps.homework.w13.search.model.DataBaseNames;
import net.javajoy.jps.homework.w13.search.view.MainFrame;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Kyselova Nataliia
 */
public class Controller extends KeyAdapter {

    public static final String ACTION_MAP_KEY = "actionMapKey";

    private DataBaseNames base;
    private MainFrame frame;
    private JLabel labelTips;
    private JTextField textFieldName;

    public Controller(MainFrame mainFrame, DataBaseNames base) {
        this.base = base;
        this.frame = mainFrame;
        defineComponents();
        initKeyListener();
        initCaretListener();
    }

    private void defineComponents() {
        this.textFieldName = frame.getTextFieldName();
        this.labelTips = frame.getLabelTips();
    }

    private void initKeyListener() {
        KeyAdapter keyAdapter = new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    textFieldName.setBackground(Color.WHITE);
                    textFieldName.setText("");
                    labelTips.setText(" ");
                }

                Action action = new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        textFieldName.setBackground(Color.WHITE);
                        textFieldName.setText(labelTips.getText());

                    }
                };
                textFieldName.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE,
                        java.awt.event.InputEvent.CTRL_DOWN_MASK), ACTION_MAP_KEY);
                textFieldName.getActionMap().put(ACTION_MAP_KEY, action);

            }
        };

        textFieldName.addKeyListener(keyAdapter);
    }

    private void initCaretListener() {
        textFieldName.addCaretListener(caretListener);
    }

    private CaretListener caretListener = new CaretListener() {

        @Override
        public void caretUpdate(CaretEvent e) {
            String text = "";
            textFieldName.setBackground(Color.WHITE);
            text = textFieldName.getText();
            String tipText = defineTip(text);
            if (tipText == null) {
                textFieldName.setBackground(Color.RED);
            }
            if ((tipText == null || text.length() == 0)) {
                labelTips.setText(" ");
            } else {
                labelTips.setText(tipText);
            }
        }
    };

    private String defineTip(String text) {
        for (String element : base.getStorage()) {
            if (text.length() <= element.length()) {
                if (text.equals(element.substring(0, text.length()))) {
                    return element;
                }
            }
        }
        return null;
    }
}
