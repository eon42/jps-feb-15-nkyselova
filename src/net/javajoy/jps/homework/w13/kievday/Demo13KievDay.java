package net.javajoy.jps.homework.w13.kievday;

import java.util.Calendar;
import java.util.Scanner;

/**
 * @author Kiselova Nataliia
 */
public class Demo13KievDay {

    public static void main(String[] args) {
        startDefining();
    }

    private static void startDefining() {
        showWelcomeMessage();
        int year = inputValidate();

        Calendar date = new Date().getLastSaturday(year);

        System.out.printf("In %04d year the Day of Kiev is %2d of May", year, date.get(Calendar.DAY_OF_MONTH));
    }

    private static int inputValidate() {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String pattern = "([0-9]+)";
        while (!s.matches(pattern)) {
            System.out.print("Wrong year. Please, try again\n> ");
            s = scanner.nextLine();
        }
        return Integer.parseInt(s);
    }

    private static void showWelcomeMessage() {
        System.out.print("Please, input year to define the Day of Kiev\n> ");
    }

}
