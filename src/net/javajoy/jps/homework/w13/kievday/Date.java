package net.javajoy.jps.homework.w13.kievday;

import java.util.Calendar;

/**
 * @author Kiselova Nataliia
 */
public class Date {

    private Calendar date = Calendar.getInstance();

    public Calendar getLastSaturday(int year) {
        int month = Calendar.MAY;
        int day = 31;
        date.set(year, month, day);
        while (date.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
            date.set(year, month, --day);
        }
        return date;

        /*
        //AN: более короткий вариант
        if (date.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
            date.set(Calendar.DAY_OF_MONTH, day - date.get(Calendar.DAY_OF_WEEK));
        return date;
        */
    }
}
