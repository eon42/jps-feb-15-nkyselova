package net.javajoy.jps.homework.w05;

import java.util.NoSuchElementException;

/**
 * @author Kiselova Nataliia
 */
public class Main {
    public static final int ARRAY_LENGTH = 8;

    public static void main(String[] args) {
        //testMethodsInStringArrayData();
        //testIteratorFirstCondition();
        //testIteratorSecondCondition();
        testIteratorExceptionCondition();
    }

    private static void testIteratorFirstCondition() {
        StringArrayData array = createArray(ARRAY_LENGTH);
        array = fillArray(array);
        array.print();


        System.out.println("[SET CONDITION - *How*]");
        array.setCondition(new Condition() {
            String pattern = ".*How.*";

            @Override
            public boolean check(String string) {
                return string.matches(pattern);
            }
        });

        StringArrayData.Iterator iterator = array.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println();
        System.out.println("************************************************************");
        System.out.println();
    }

    private static void testIteratorSecondCondition() {
        StringArrayData array = createArray(ARRAY_LENGTH);
        array = fillArray(array);
        array.print(true);

        System.out.println("[SET CONDITION - LENGTH > 30]");
        array.setCondition(new Condition() {
            int length = 30;

            @Override
            public boolean check(String string) {
                return string.length() > length;
            }
        });

        StringArrayData.Iterator iterator = array.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println();
        System.out.println("************************************************************");
        System.out.println();
    }

    private static void testIteratorExceptionCondition() {
        StringArrayData array = createArray(ARRAY_LENGTH);
        array = fillArray(array);
        array.updateByIndex("", 2);
        StringArrayData.Iterator iterator = array.iterator();

        array.setCondition(new Condition() {

            // AS: не совсем удачная реализация. Имелось в виду, что исключение выбрасывается при наступлении определенной ситуации
            /*
            @Override
            public boolean check(String string) throws ConditionCantBeChecked {
                throw new ConditionCantBeChecked();
            }
            */
            @Override
            public boolean check(String string) throws ConditionCantBeChecked {
                // AS: например такой
                if (string.length() == 0) {
                    throw new ConditionCantBeChecked();
                }
                return true;
            }
        });

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println();
        System.out.println("************************************************************");
        System.out.println();
    }

    private static void testMethodsInStringArrayData() {
        StringArrayData array = createArray(ARRAY_LENGTH);
        System.out.println("[FILL ARRAY]");
        array = fillArray(array);
        array.print();

        System.out.println("[UPDATE FIRST]");
        array.updateFirst("And pour the waters of the Nile", "And pour the waters of the Nile-Nile-Nile");
        array.print();

        System.out.println("[UPDATE ALL]");
        array.updateAll("How cheerfully he seems to grin", "How cheerfully he seems to grin-grin-grin");
        array.print();

        System.out.println("[DELETE FIRST]");
        array.deleteFirst("And pour the waters of the Nile-Nile-Nile");
        array.print();

        array.deleteAll("How cheerfully he seems to grin-grin-grin");
        array.print();

        System.out.println("[ADD TO POSITIONS 3, 5, 8]");
        array.add("And pour the waters of the Nile", 3);
        array.add("How cheerfully he seems to grin", 5);
        array.add("With gently smiling jaws! ", 8);
        array.print();

        // AS: будет если array.add("some string", -2); ?
        // Также обрати внимание на этот момент в остальных методах где есть работа с индексом.

        try {
            System.out.println("[ADD TO POSITION ARRAY_LENGTH+10]");
            array.add("With gently smiling jaws! ", ARRAY_LENGTH + 10);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Warning! Handling an invalid array index!");
        }

        try {
            System.out.println("[UPDATE FIRST]");
            array.updateFirst("La-la-la", "Some string");
        } catch (NoSuchElementException e) {
            System.out.println("Warning! There is no such element in array");
        }
        System.out.println();
        System.out.println("************************************************************");
        System.out.println();
    }


    private static StringArrayData createArray(int n) {
        return new StringArrayData(new String[n]);
    }

    // AS: можно обойтись без параметра
    private static StringArrayData fillArray(StringArrayData array) {
        array.add("How doth the little crocodile");
        array.add("Improve his shining tail,");
        array.add("And pour the waters of the Nile");
        array.add("On every golden scale!");
        array.add("How cheerfully he seems to grin");
        array.add("How neatly spreads his claws,");
        array.add("How cheerfully he seems to grin");//Одинаковая строка для теста
        array.add("And welcomes little fishes in,");
        array.add("With gently smiling jaws! ");
        return array;
    }

}
