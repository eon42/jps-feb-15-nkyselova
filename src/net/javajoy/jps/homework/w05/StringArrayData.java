package net.javajoy.jps.homework.w05;

import java.util.NoSuchElementException;

/**
 * @author Kiselova Nataliia
 */
public class StringArrayData {

    private Condition condition;
    private String[] items;
    private int size;

    /**
     * Constructor
     */
    public StringArrayData() {

    }

    /**
     * Constructor
     *
     * @param items
     */
    public StringArrayData(String[] items) {
        this.items = new String[items.length];
        System.arraycopy(items, 0, this.items, 0, items.length);
        this.compress();
        for (int i = 0; i < items.length; i++) {
            if (items[i] != null) {
                size = i - 1;
            }
        }
    }

    /**
     * Create array with length, which is equals param length
     *
     * @param length
     * @return array
     */
    public String[] createArray(int length) {
        return new String[length];
    }

    /**
     * Add String item in thw end in array. If array is already filled, method do nothing
     *
     * @param item
     */
    public void add(String item) {
        if (size < this.items.length) {
            this.items[size] = item;
            size++;
        }
    }

    /**
     * Add String item in n place in array. If array is already filled, method throw ArrayIndexOutOfBoundsException
     *
     * @param item string, that should be added
     * @param n    consecutive number, starting at 1
     */
    public void add(String item, int n) {
        if (n > this.items.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (this.items[this.items.length - 1] == null) {
            System.arraycopy(this.items, n - 1, this.items, n, this.items.length - 1 - (n - 1));
            this.items[n - 1] = item;
            size++;
        }
    }

    /**
     * Update all occurrence itemOld to itemNew.
     *
     * @param itemOld String
     * @param itemNew String
     */
    public void updateAll(String itemOld, String itemNew) {
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i].equals(itemOld)) {
                this.items[i] = itemNew;
            }
        }
    }

    /**
     * Update first occurrence itemOld to itemNew. If itemOld not found, method throw ArrayIndexOutOfBoundsException
     *
     * @param itemOld String
     * @param itemNew String
     */
    public void updateFirst(String itemOld, String itemNew) {
        boolean found = false;
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i].equals(itemOld)) {
                this.items[i] = itemNew;
                found = true;
                break;
            }
        }
        if (!found) {
            throw new NoSuchElementException();
        }
    }

    /**
     * Rewrite n place in array String item.
     *
     * @param item string, that should be added
     * @param n    consecutive number, starting at 0
     */
    public void updateByIndex(String item, int n) {
        this.items[n] = item;
    }

    /**
     * Delete all founded elements in array equals String item
     *
     * @param item String
     */
    public void deleteAll(String item) {
        for (int i = 0; i < size; i++) {
            if (this.items[i].equals(item)) {
                this.items[i] = null;
                size--;
            }
        }

        this.compress();
    }

    /**
     * Delete first founded element in array equals String item
     *
     * @param item String
     */
    public void deleteFirst(String item) {
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i].equals(item)) {
                this.items[i] = null;
                size--;
                break;
            }
        }

        this.compress();
    }

    /**
     * Rewrites all not null elements closer to the beginning of array if there before them null elements.
     */
    public void compress() {
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i] == null) {
                System.arraycopy(this.items, i + 1, this.items, i, this.items.length - 1 - i);
                this.items[this.items.length - 1] = null;
            }
        }
    }

    /**
     * Print array
     */
    public void print() {
        print(false);
    }

    public void print(boolean showStringSize) {
        for (String item : this.items) {
            System.out.print(item);
            if (showStringSize) {
                System.out.print(" (" + item.length() + ")");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Getter
     *
     * @return array items
     */
    public String[] getItems() {
        return items;
    }

    /**
     * Set the condition that should be checked
     *
     * @param condition
     */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    /**
     * Class Iterator
     */
    public class Iterator {
        private int position;

        /**
         * Method, that returns next item in array, if they satisfy the condition. Throw NoSuchElementException.
         *
         * @return next item in array
         */
        public String next() {
            while (position < items.length) {
                try {
                    if (condition.check(items[position])) {
                        return items[position++];
                    }
                } catch (ConditionCantBeChecked conditionCantBeChecked) {
                    System.out.println("Warning! Condition can't be checked");
                }
            }
            return null;
        }

        /**
         * Method, that return if there is item next, which satisfy the condition.
         *
         * @return true, if there is item next
         */
        public boolean hasNext() {
            while (position < items.length) {
                try {
                    if (condition.check(items[position])) {
                        return true;
                    } else {
                        position++;
                    }
                } catch (ConditionCantBeChecked conditionCantBeChecked) {
                    System.out.println("Warning! Condition can't be checked");
                    position++;
                }

            }
            return false;
        }
    }

    /**
     * Constructor fot Iterator.
     *
     * @return new Iterator
     */
    public Iterator iterator() {
        return new Iterator();
    }
}

