package net.javajoy.jps.homework.w05;

/**
 * Abstract class with one method check()
 *
 * @author Kiselova Nataliia
 */
public abstract class Condition {
    /**
     * Check weather the given {@code elem} satisfies the condition or not
     *
     * @return boolean true, if instance satisfy the condition
     */
    abstract boolean check(String elem) throws ConditionCantBeChecked;
}
