package net.javajoy.jps.homework.w05;

/**
 * New exception class
 *
 * @author Kiselova Nataliia
 */
public class ConditionCantBeChecked extends Exception {

    /**
     * {@inheritDoc}
     */
    public ConditionCantBeChecked() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessage() {
        return "Исключение \"Условие нельзя проверить\" " + super.getMessage();
    }
}
