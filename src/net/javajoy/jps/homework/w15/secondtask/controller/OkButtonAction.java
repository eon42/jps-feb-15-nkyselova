package net.javajoy.jps.homework.w15.secondtask.controller;

import net.javajoy.jps.homework.w15.secondtask.view.MainFrame;
import net.javajoy.jps.homework.w15.secondtask.view.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;

/**
 * @author Kyselova Nataliia
 */
public class OkButtonAction implements ActionListener {

    private MainPanel panel;
    private JTextField pathToFileField;
    private JTextField pathToAnotherFileField;
    private JComboBox<String> comboBox;

    public OkButtonAction(MainFrame frame) {
        this.panel = (MainPanel) frame.getContentPane();
        this.pathToFileField = ((MainPanel) frame.getContentPane()).getPathToFileField();
        this.pathToAnotherFileField = ((MainPanel) frame.getContentPane()).pathToAnotherFileField();
        this.comboBox = ((MainPanel) frame.getContentPane()).getComboBox();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String item = (String) comboBox.getSelectedItem();
        switch (item) {
            case MainPanel.COPY:
                copy(true);
                break;
            case MainPanel.MOVE:
                move();
                break;
            case MainPanel.REMOVE:
                remove();
                break;
            case MainPanel.GET_SIZE:
                getSize();
                break;
            default:
                break;
        }
    }

    //AN: рекомендую сделать отдельный класс FileManager в котором будут все эти методы ниже. Не обязательно делать эти метода там статическими
    private void copy(boolean showDoneMassage) {
        File srcFolder = getFilePath(pathToFileField, false, false);
        File destFolder = getFilePath(pathToAnotherFileField, false, true);

        if (srcFolder == null || !srcFolder.exists()) {
            JOptionPane.showMessageDialog(panel, "Source file or directory does not exist");
        } else {
            if (destFolder == null) {
                JOptionPane.showMessageDialog(panel, "Wrong path where to copy");
            } else {
                doCopy(showDoneMassage, srcFolder, destFolder);
            }
        }
    }

    private void doCopy(boolean showDoneMassage, File srcFolder, File destFolder) {
        try {
            copyFolder(srcFolder, destFolder);
            if (showDoneMassage) {
                JOptionPane.showMessageDialog(panel, "Done");
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(panel, "Error! Operation canceled");
        }
    }

    private void move() {
        copy(false);
        remove();
    }

    private void remove() {
        File file = getFilePath(pathToFileField, true, false);
        if (file != null) {
            deleteDir(file);
            JOptionPane.showMessageDialog(panel, "Done");
        }
    }

    public void getSize() {
        File file = getFilePath(pathToFileField, true, false);
        if (file != null && file.exists()) {
            panel.setInformLabelText(String.valueOf(sizeFile(file, 0)));
        }
    }

    private File getFilePath(JTextField path, boolean showWarnMassage, boolean createPath) {
        String s = path.getText();
        File file = new File(s);

        if (!file.exists()) {
            if (showWarnMassage) {
                JOptionPane.showMessageDialog(panel, "File or directory does not exists");
            }
            if (createPath) {
                return file;
            }
            return null;
        } else {
            return file;
        }
    }

    private long sizeFile(File f, int depth) {
        if (f.isDirectory()) {
            long sz = 0;
            File[] fileList = f.listFiles();

            if (fileList == null)
                return  -1;

            for (File child : fileList)
                sz += sizeFile(child, depth + 1);
            return sz;
        } else {
            return f.length();
        }
    }

    /**
     *
     * @param dir - which to delete
     * @return true if deletion was successful, otherwise false
     */
    private boolean deleteDir(File dir) {
        try {
            if (dir.isDirectory()) {
                File[] fileList = dir.listFiles();
                if (fileList == null)
                    return false;

                for (File f : fileList)
                    deleteDir(f);
            }
            return dir.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdir();
            }

            String files[] = src.list();

            for (String file : files) {
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                copyFolder(srcFile, destFile);
            }
        } else {
            try (OutputStream out = new FileOutputStream(dest)) {
                Files.copy(src.toPath(), out);
            }
        }
    }
}
