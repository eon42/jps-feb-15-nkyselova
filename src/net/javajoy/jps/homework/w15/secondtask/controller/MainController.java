package net.javajoy.jps.homework.w15.secondtask.controller;

import net.javajoy.jps.homework.w15.secondtask.view.MainFrame;
import net.javajoy.jps.homework.w15.secondtask.view.MainPanel;

/**
 * @author Kyselova Nataliia
 */
public class MainController {

    public MainController() {
        MainFrame frame = new MainFrame();
        MainPanel panel = ((MainPanel) frame.getContentPane());
        panel.getComboBox().addActionListener(new ComboBoxAction(frame));
        panel.getOkButton().addActionListener(new OkButtonAction(frame));
    }
}
