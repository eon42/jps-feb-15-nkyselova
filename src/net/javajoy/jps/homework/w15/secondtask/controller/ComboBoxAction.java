package net.javajoy.jps.homework.w15.secondtask.controller;

import net.javajoy.jps.homework.w15.secondtask.view.MainFrame;
import net.javajoy.jps.homework.w15.secondtask.view.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Kyselova Nataliia
 */
public class ComboBoxAction implements ActionListener {

    private JLabel informLabel;

    public ComboBoxAction(MainFrame frame) {
        this.informLabel = ((MainPanel) frame.getContentPane()).getInformLabel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox box = (JComboBox) e.getSource();
        String item = (String) box.getSelectedItem();

        String textInformLabel = "";
        switch (item) {
            case MainPanel.COPY:
                textInformLabel = "Copy to";
                break;
            case MainPanel.MOVE:
                textInformLabel = "Move to";
                break;
        }
        informLabel.setText(textInformLabel);
    }
}
