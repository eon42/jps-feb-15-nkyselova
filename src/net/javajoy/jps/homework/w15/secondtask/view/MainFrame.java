package net.javajoy.jps.homework.w15.secondtask.view;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class MainFrame extends JFrame {

    public MainFrame() {
        initFrame();
    }

    private void initFrame() {
        MainPanel panel = new MainPanel();
        setPreferredSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("W15.2");
        setResizable(false);
        setContentPane(panel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
