package net.javajoy.jps.homework.w15.secondtask.view;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class MainPanel extends JPanel {

    //AN: можно сделать список констант через Enum
    public static final String EMPTY = "";
    public static final String COPY = "Copy";
    public static final String MOVE = "Move";
    public static final String REMOVE = "Remove";
    public static final String GET_SIZE = "Get size";

    private JTextField pathToFileField;
    private JTextField pathToAnotherFileField;
    private JComboBox<String> comboBox;
    private JButton okButton;
    private JLabel informLabel;

    public MainPanel() {
        setLayout(new GridLayout(7, 1));

        add(new JLabel("Please, input file or directory name"));

        pathToFileField = new JTextField();
        add(pathToFileField);

        add(new JLabel("Please, choose action"));

        String[] items = {
                EMPTY,
                COPY,
                MOVE,
                REMOVE,
                GET_SIZE,
        };
        comboBox = new JComboBox<>(items);
        comboBox.setSelectedIndex(0);
        add(comboBox);

        informLabel = new JLabel();
        add(informLabel);

        pathToAnotherFileField = new JTextField();
        add(pathToAnotherFileField);

        okButton = new JButton("Ok");
        add(okButton, 6);
    }

    public void setInformLabelText(String text) {
        informLabel.setText(text);
    }

    public JComboBox<String> getComboBox() {
        return comboBox;
    }

    public JTextField getPathToFileField() {
        return pathToFileField;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JTextField pathToAnotherFileField() {
        return pathToAnotherFileField;
    }

    public JLabel getInformLabel() {
        return informLabel;
    }
}
