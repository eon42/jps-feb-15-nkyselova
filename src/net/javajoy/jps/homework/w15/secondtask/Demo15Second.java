package net.javajoy.jps.homework.w15.secondtask;

import net.javajoy.jps.homework.w15.secondtask.controller.MainController;

/**
 * @author Kyselova Nataliia
 */
public class Demo15Second {

    //AN: Почти все хорошо. Пару моментов только подправить
    public static void main(String[] args) {
        new MainController();
    }
}
