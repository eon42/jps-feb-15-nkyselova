package net.javajoy.jps.homework.w15.puzzle.controller.actions;

import net.javajoy.jps.homework.w15.puzzle.view.Panel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * @author Kyselova Nataliia
 */
public class HelpAction extends AbstractAction {
    private Panel panel;

    public HelpAction(final Panel panel) {
        this.panel = panel;
        putValue(Action.NAME, "Help");
        putValue(Action.SMALL_ICON, new ImageIcon("img/help.jpg"));
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_H);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_DOWN_MASK));
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        showHelpDialog(panel);
    }

    private void showHelpDialog(Panel panel) {
        JOptionPane.showMessageDialog(panel.getRootPane(), "How to control:\n" +
                "Mouse: choose the picture and choose another to swap\n" +
                "Keyboard: click \"Left\", \"Right\", \"Up\", \"Down\" to move on pictures \n" +
                "and \"Space\" to choose the picture and choose another to swap");
    }
}
