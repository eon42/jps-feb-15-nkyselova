package net.javajoy.jps.homework.w15.puzzle.controller.actions;

import net.javajoy.jps.homework.w15.puzzle.Demo15;
import net.javajoy.jps.homework.w15.puzzle.model.Matrix;
import net.javajoy.jps.homework.w15.puzzle.view.Panel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Kyselova Nataliia
 */
public class SaveAsSerializableAction extends AbstractAction {
    public static final int WIDTH = Demo15.GRID_DIMENSION_HORIZONTAL;
    public static final int HEIGHT = Demo15.GRID_DIMENSION_VERTICAL;
    public static final String SAVE_AS = "Save as...";

    private Panel panel;

    public SaveAsSerializableAction(Panel panel) {
        this.panel = panel;
        putValue(Action.NAME, SAVE_AS);
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        int confirmation = fileChooser.showDialog(panel, SAVE_AS);

        if (confirmation == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                write(oos, Matrix.getInstance(WIDTH, HEIGHT));
                JOptionPane.showMessageDialog(panel, "Data have been saved");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(panel, "Error! Data didn't save");
        }
    }

    private void write(ObjectOutputStream oos, Matrix matrix) {
        try {
            oos.writeByte(WIDTH);
            oos.writeByte(HEIGHT);
            for (int i = 0; i < WIDTH; i++) {
                for (int j = 0; j < HEIGHT; j++) {
                    oos.writeInt(matrix.getElement(i, j));
                }
            }
            oos.writeInt(panel.getChosenBoarder().getRGB());
            oos.writeInt(panel.getBackground().getRGB());
            oos.flush();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
