package net.javajoy.jps.homework.w15.puzzle.controller.actions;

import net.javajoy.jps.homework.w15.puzzle.view.ChooseColorFrame;
import net.javajoy.jps.homework.w15.puzzle.view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Kyselova Nataliia
 */
public class ChooseColorAction extends AbstractAction {
    private net.javajoy.jps.homework.w15.puzzle.view.Panel panel;
    private Color firstColor;
    private Color secondColor;
    private Color thirdColor;
    private Color forthColor;
    private Boolean isMarquee;
    private ChooseColorFrame chooseColorFrame;

    public ChooseColorAction(String name, String pathImage, Object mnemonicKey, Object acceleratorKey,
                             Panel panel, Color first, Color second, Color third, Color forth, Boolean isMarquee) {
        this.panel = panel;
        this.firstColor = first;
        this.secondColor = second;
        this.thirdColor = third;
        this.forthColor = forth;
        this.isMarquee = isMarquee;
        putValue(Action.NAME, name);
        putValue(Action.SMALL_ICON, new ImageIcon(pathImage));
        putValue(Action.MNEMONIC_KEY, mnemonicKey);
        putValue(Action.ACCELERATOR_KEY, acceleratorKey);
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        chooseColorFrame = new ChooseColorFrame(firstColor, secondColor, thirdColor, forthColor);
        chooseColorFrame.getOkButton().addActionListener(getListener(panel));
    }

    private ActionListener getListener(Panel panel) {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color color = forthColor;
                if (chooseColorFrame.getFirstColor().isSelected()) {
                    color = firstColor;
                } else if (chooseColorFrame.getSecondColor().isSelected()) {
                    color = secondColor;
                } else if (chooseColorFrame.getThirdColor().isSelected()) {
                    color = thirdColor;
                } else if (chooseColorFrame.getForthColor().isSelected()) {
                    color = forthColor;
                }

                if (isMarquee) {
                    panel.repaintPanel(color);
                } else {
                    panel.setBackground(color);
                }
                chooseColorFrame.getFrameColor().dispose();
            }
        };
    }
}
