package net.javajoy.jps.homework.w15.puzzle.controller.actions;

import net.javajoy.jps.homework.w15.puzzle.Demo15;
import net.javajoy.jps.homework.w15.puzzle.model.Matrix;
import net.javajoy.jps.homework.w15.puzzle.view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author Kyselova Nataliia
 */
public class OpenSerializableAction extends AbstractAction {

    public static final int WIDTH = Demo15.GRID_DIMENSION_HORIZONTAL;
    public static final int HEIGHT = Demo15.GRID_DIMENSION_VERTICAL;
    public static final String OPEN = "Open";

    private Panel panel;
    private Color chosenBoarder;
    private File file;

    public OpenSerializableAction(Panel panel) {
        this.panel = panel;
        putValue(Action.NAME, OPEN);
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        int confirmation = fileChooser.showDialog(panel, OPEN);

        if (confirmation == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();
        }

        if (file == null || !file.exists()) {
            JOptionPane.showMessageDialog(panel, String.format("File '%s' to load data from does not exist", file));
            return;
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            loadData(ois, Matrix.getInstance(WIDTH, HEIGHT));
            JOptionPane.showMessageDialog(panel, "Data have been loaded");
            panel.repaintPanel(chosenBoarder);
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(panel, "Error! Data didn't load");
        }
    }

    private void loadData(ObjectInputStream ois, Matrix matrix) {
        try {
            int width = ois.readByte();
            int height = ois.readByte();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    matrix.setElement(i, j, ois.readInt());
                }
            }
            chosenBoarder = new Color(ois.readInt());
            panel.setChosenBoarder(chosenBoarder);
            panel.setBackground(new Color(ois.readInt()));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public File getFile() {
        return file;
    }
}
