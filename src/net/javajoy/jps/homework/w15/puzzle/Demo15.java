package net.javajoy.jps.homework.w15.puzzle;

import net.javajoy.jps.homework.w15.puzzle.controller.GameMouseListener;
import net.javajoy.jps.homework.w15.puzzle.controller.KeyboardListener;
import net.javajoy.jps.homework.w15.puzzle.model.Matrix;
import net.javajoy.jps.homework.w15.puzzle.view.MainFrame;

/**
 * @author Kiselova Nataliia
 */

public class Demo15 {
    public static final int GRID_DIMENSION_VERTICAL = 3;
    public static final int GRID_DIMENSION_HORIZONTAL = 4;
    public static final int FIELD_SIZE_IN_PIXEL = 100;

    public static void main(String[] args) {
        new MainFrame(
                Matrix.getInstance(GRID_DIMENSION_VERTICAL, GRID_DIMENSION_HORIZONTAL),
                new KeyboardListener(), new GameMouseListener()
        );
    }


    //AN: Эти классы одинаковые. Либо оставить один нужно, либо сделать один базовый и два наследника. Нужно выделить что у них разного
    //AN: я пока что не понял что у них разного.
    /*
        SaveAsSerializableAction.java
        SaveSerializableAction.java
    */
    //KN: ок, оставляю SaveAsSerializableAction.java
}
