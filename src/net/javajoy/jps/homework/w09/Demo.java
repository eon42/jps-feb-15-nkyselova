package net.javajoy.jps.homework.w09;

import net.javajoy.jps.homework.w09.om.Record;
import net.javajoy.jps.homework.w09.om.Time;

import java.util.Map;
import java.util.Scanner;

/**
 * @author Kiselova Nataliia
 */
public class Demo {
    public static final String STOP_CODE = "stop";
    public static final int VIEW = 1;
    public static final int ADD = 2;
    public static final int REMOVE = 3;
    public static final int EXIT = 4;

    public static void main(String[] args) {
        notebookTest();
    }

    private static void notebookTest() {
        Notebook notebook = new Notebook();
        Scanner scanner = new Scanner(System.in);

        showWelcomeMessage();
        int input = chooseOption(scanner);
        while (input != EXIT) {
            switch (input) {
                case VIEW:
                    printRecords(notebook);
                    break;
                case ADD:
                    notebook = addRecords(notebook, scanner);
                    break;
                case REMOVE:
                    notebook = removeRecords(notebook, scanner);
                    break;
                default:
                    break;
            }
            input = chooseOption(scanner);
        }
    }

    private static Notebook removeRecords(Notebook notebook, Scanner scanner) {
        String welcomeMessageForRemoving = "Enter time for removing (or \"stop\" if you change my mind)\n> ";
        Time time = readTime(scanner, welcomeMessageForRemoving);

        if (time != null) { //AN: Ok, теперь понятно
            notebook.delete(time);
            System.out.println("Record was successfully removed\n");
        } else
            System.out.println("Nothing happen\n");

        return notebook;
    }

    private static Time readTime(Scanner scanner, String welcomeMessageForRemoving) {
        System.out.print(welcomeMessageForRemoving);
        String timeString = scanner.nextLine();
        String pattern = "(([0,1][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]";

        while (!(timeString.matches(pattern) || timeString.equals(STOP_CODE))) {
            System.out.print("There is no record for this time. ");
            System.out.print(welcomeMessageForRemoving);
            timeString = scanner.nextLine();
        }

        if (timeString.equals(STOP_CODE)) return null;

        String timeTokens[] = timeString.split(":");
        Time time = new Time(); //AN: нужно объявлять переменные там где мы их используем
        time.setHour(Integer.parseInt(timeTokens[0]));
        time.setMin(Integer.parseInt(timeTokens[1]));
        time.setSec(Integer.parseInt(timeTokens[2]));

        return time;
    }

    private static Notebook addRecords(Notebook notebook, Scanner scanner) {
        System.out.print("Enter text for record\n> ");
        notebook.add(scanner.nextLine());
        System.out.println("Record was successfully added\n");
        return notebook;
    }

    private static void printRecords(Notebook notebook) {
        for (Map.Entry<Time, Record> entry : notebook.getEntries()) {
            System.out.printf("%02d:%02d:%02d %s\n", entry.getKey().getHour(), entry.getKey().getMin(),
                    entry.getKey().getSec(), entry.getValue().getText());
        }
        System.out.println();
    }

    private static void showWelcomeMessage() {
        System.out.println("Welcome to the Notebook! Here you can:");
    }

    private static void printOptions() {
        System.out.println("1. View records");
        System.out.println("2. Add records");
        System.out.println("3. Remove records");
        System.out.println("4. Exit");
        System.out.print("Please, choose one option (input digit)\n> ");
    }

    private static int chooseOption(Scanner scanner) {
        printOptions();
        String s = scanner.nextLine();
        String pattern = "([1-4])";
        while (!s.matches(pattern)) {
            printOptions();
            s = scanner.nextLine();
        }
        return Integer.parseInt(s);
    }
}
