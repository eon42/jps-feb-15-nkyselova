package net.javajoy.jps.homework.w09;

import net.javajoy.jps.homework.w09.om.Record;
import net.javajoy.jps.homework.w09.om.Time;

import java.util.*;

/**
 * @author Kiselova Nataliia
 */
public class Notebook {
    private Map<Time, Record> storage = new HashMap<>();

    /**
     * Add record to storage with text
     *
     * @param text String
     */
    public void add(String text) {
        Date d = new Date();
        storage.put(new Time(d.getHours(), d.getMinutes(), d.getSeconds()), new Record(text));
        //AN: deprecated методы для ДЗ можно использовать, но в реальном проекте лучше не используй
        //KN: ok

    }

    /**
     * Delete record which matches chosen time
     *
     * @param time
     */
    public void delete(Time time) {
        storage.remove(time);
    }

    /**
     * @return all records in notebook
     */
    public Set<Map.Entry<Time, Record>> getEntries() {
        return storage.entrySet();
    }
}
