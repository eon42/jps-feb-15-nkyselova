package net.javajoy.jps.homework.w09.om;

/**
 * @author Kiselova Nataliia
 */
public class Record {
    private String text;

    /**
     * Constructor
     *
     * @param text
     */
    public Record(String text) {
        this.text = text;
    }

    /**
     * Getter
     *
     * @return text from record
     */
    public String getText() {
        return text;
    }

    /**
     * Setter
     *
     * @param text set to Record
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "{" +
                "text='" + text + '\'' +
                '}';
    }
}
