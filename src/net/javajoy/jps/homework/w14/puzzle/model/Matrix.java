package net.javajoy.jps.homework.w14.puzzle.model;

import java.util.Observable;
import java.util.Random;

/**
 * @author Kiselova Nataliia
 */
public class Matrix extends Observable {
    public static final int UNDEFINED_VALUE = -1;
    public static final int NUMBERS_OF_TEN = 10;

    private int height;
    private int width;
    private int currentX = UNDEFINED_VALUE;
    private int currentY = UNDEFINED_VALUE;
    private int preCurrentX = UNDEFINED_VALUE;
    private int preCurrentY = UNDEFINED_VALUE;
    private int chosenX = UNDEFINED_VALUE;
    private int chosenY = UNDEFINED_VALUE;
    private int preChosenX = UNDEFINED_VALUE;
    private int preChosenY = UNDEFINED_VALUE;

    private boolean nothingWasPressed;
    private int[][] matrix;

    private static Matrix instance;

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public static Matrix getInstance(int gridDimensionHorizontal, int gridDimensionVertical) {
        if (instance == null) {
            instance = new Matrix(gridDimensionHorizontal, gridDimensionVertical);
        }
        return instance;
    }

    private Matrix(int width, int height) {
        this.width = width;
        this.height = height;
        matrix = fillMatrix(width, height);
        setNothingWasPressed(true);
    }

    private int[][] fillMatrix(int width, int height) {
        matrix = new int[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                matrix[i][j] = (i + 1) * NUMBERS_OF_TEN + j + 1;
            }
        }

        return matrix;
    }

    public Matrix shuffle() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int randomI = new Random().nextInt(width);
                int randomJ = new Random().nextInt(height);
                this.swap(i, j, randomI, randomJ);
            }
        }
        return this;
    }

    public Matrix swap(int iFirst, int jFirst, int iSecond, int jSecond) {
        int temp = this.matrix[iFirst][jFirst];
        this.matrix[iFirst][jFirst] = this.matrix[iSecond][jSecond];
        this.matrix[iSecond][jSecond] = temp;

        setChanged();
        notifyObservers(matrix);

        return this;
    }

    public boolean isGameOver() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (matrix[i][j] != (i + 1) * 10 + j + 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setFirstCell() {
        setPreCurrentX(getCurrentX());
        setPreCurrentY(getCurrentY());
        setCurrentX(0);
        setCurrentY(0);
        setNothingWasPressed(false);
        setChanged();
        notifyObservers(matrix);
    }


    public void setSource(int currentX, int currentY) {
        setPreChosenX(getChosenX());
        setPreChosenY(getChosenY());
        setChosenX(currentX);
        setChosenY(currentY);
        setChanged();
        notifyObservers(matrix);
    }

    public void renewCurrent(int x, int y) {
        setPreCurrentX(getCurrentX());
        setPreCurrentY(getCurrentY());
        setCurrentX(x);
        setCurrentY(y);
    }

    public void renewChosen(int x, int y) {
        setPreChosenX(getChosenX());
        setPreChosenY(getChosenY());
        setChosenX(x);
        setChosenY(y);
    }

    public boolean isMatch(int currentX, int currentY) {
        return (getChosenX() == currentX) && (getChosenY() == currentY);
    }

    public boolean isSet() {
        return !((getChosenX() == UNDEFINED_VALUE) && (getChosenY() == UNDEFINED_VALUE));
    }

    public void doOnClick() {
        if (isSet()) {
            if (!isMatch(getCurrentX(), getCurrentY())) {
                swap(getChosenX(), getChosenY(), getCurrentX(), getCurrentY());
            }
            renewChosen(UNDEFINED_VALUE, UNDEFINED_VALUE);
        } else {
            setSource(getCurrentX(), getCurrentY());
        }

        setChanged();
        notifyObservers(matrix);
    }

    public int getChosenX() {
        return chosenX;
    }

    public void setChosenX(int chosenX) {
        this.chosenX = chosenX;
        setChanged();
        notifyObservers(matrix);
    }

    public int getChosenY() {
        return chosenY;
    }

    public void setChosenY(int chosenY) {
        this.chosenY = chosenY;
        setChanged();
        notifyObservers(matrix);
    }

    public int getCurrentX() {
        return currentX;
    }

    public int getCurrentY() {
        return currentY;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
        setChanged();
        notifyObservers(matrix);
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
        setChanged();
        notifyObservers(matrix);
    }

    public boolean isNothingWasPressed() {
        return nothingWasPressed;
    }

    public boolean isDefinedCell(int x, int y) {
        return x != UNDEFINED_VALUE && y != UNDEFINED_VALUE;
    }

    public void setNothingWasPressed(boolean nothingWasPressed) {
        this.nothingWasPressed = nothingWasPressed;
    }

    public boolean isCurrent() {
        return !((getCurrentX() == UNDEFINED_VALUE) && (getCurrentY() == UNDEFINED_VALUE));
    }

    public void setPreChosenX(int preChosenX) {
        this.preChosenX = preChosenX;
        setChanged();
        notifyObservers(matrix);
    }

    public void setPreChosenY(int preChosenY) {
        this.preChosenY = preChosenY;
        setChanged();
        notifyObservers(matrix);
    }

    public int getPreChosenX() {
        return preChosenX;
    }

    public int getPreChosenY() {
        return preChosenY;
    }

    public void setPreCurrentX(int preCurrentX) {
        this.preCurrentX = preCurrentX;
        setChanged();
        notifyObservers(matrix);
    }

    public void setPreCurrentY(int preCurrentY) {
        this.preCurrentY = preCurrentY;
        setChanged();
        notifyObservers(matrix);
    }

    public int getPreCurrentX() {
        return preCurrentX;
    }

    public int getPreCurrentY() {
        return preCurrentY;
    }

    public int getElement(int i, int j) {
        return this.matrix[j][i];
    }

    public void setElement(int i, int j, int value) {
        this.matrix[j][i] = value;
    }
}

