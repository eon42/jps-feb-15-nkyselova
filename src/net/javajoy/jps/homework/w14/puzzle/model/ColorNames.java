package net.javajoy.jps.homework.w14.puzzle.model;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kyselova Nataliia
 */
public class ColorNames {
    private Map<Color, String> colorMap;

    public ColorNames() {
        colorMap = new HashMap<>();
        colorMap.put(Color.WHITE, "White");
        colorMap.put(Color.LIGHT_GRAY, "Grey");
        colorMap.put(Color.GREEN, "Green");
        colorMap.put(Color.YELLOW, "Yellow");
        colorMap.put(Color.RED, "Red");
    }

    public String getTextColorName(Color color) {
        return colorMap.get(color);
    }
}