package net.javajoy.jps.homework.w14.puzzle;

import net.javajoy.jps.homework.w14.puzzle.controller.GameMouseListener;
import net.javajoy.jps.homework.w14.puzzle.controller.KeyboardListener;
import net.javajoy.jps.homework.w14.puzzle.model.Matrix;
import net.javajoy.jps.homework.w14.puzzle.view.MainFrame;

/**
 * @author Kiselova Nataliia
 */

public class Demo14 {
    public static final int GRID_DIMENSION_VERTICAL = 3;
    public static final int GRID_DIMENSION_HORIZONTAL = 4;
    public static final int FIELD_SIZE_IN_PIXEL = 100;

    public static void main(String[] args) {
        new MainFrame(
                Matrix.getInstance(GRID_DIMENSION_VERTICAL, GRID_DIMENSION_HORIZONTAL),
                new KeyboardListener(), new GameMouseListener()
        );
    }
}
