package net.javajoy.jps.homework.w14.puzzle.view;

import net.javajoy.jps.homework.w14.puzzle.Demo14;
import net.javajoy.jps.homework.w14.puzzle.controller.GameMouseListener;
import net.javajoy.jps.homework.w14.puzzle.model.Matrix;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Kiselova Nataliia
 */
public class Panel extends JPanel implements Observer {
    public static final int THICKNESS = 3;
    public static final String IMAGE_PATH_PATTERN = "img/%d.jpg";

    private Color chosenBoarder;
    private Matrix matrix;
    private GameMouseListener gameMouseListener;

    public Panel(Matrix matrix, GameMouseListener gameMouseListener) {
        this.matrix = matrix;
        this.gameMouseListener = gameMouseListener;
        this.setBackground(Color.WHITE);
        this.setChosenBoarder(Color.RED);
        initPanel();
        matrix.shuffle();
        initFields();
        subscribeOn(matrix);
    }

    public void repaintPanel(Color chosenBoarder) {
        this.matrix = Matrix.getInstance(Demo14.GRID_DIMENSION_HORIZONTAL, Demo14.GRID_DIMENSION_VERTICAL);
        this.chosenBoarder = chosenBoarder;
        removeAll();
        initFields();
        subscribeOn(matrix);
    }

    public void renewPanel(Matrix matrix, GameMouseListener gameMouseListener, Color chosenBoarder) {
        this.matrix = matrix;
        this.gameMouseListener = gameMouseListener;
        this.chosenBoarder = chosenBoarder;
        matrix.shuffle();
        removeAll();
        initFields();
        subscribeOn(matrix);
    }

    private void initPanel() {
        setLayout(new GridBagLayout());

        int width = (matrix.getHeight() + MainFrame.PADDING_IN_PIXELS) * Demo14.FIELD_SIZE_IN_PIXEL;
        int height = matrix.getWidth() * Demo14.FIELD_SIZE_IN_PIXEL;

        setPreferredSize(new Dimension(width, height));
        setMinimumSize(new Dimension(width, height));
    }

    private void initFields() {
        for (int i = 0; i < matrix.getWidth(); i++) {
            for (int j = 0; j < matrix.getHeight(); j++) {
                addCell(i, j);
            }
        }
    }

    private void addCell(int x, int y) {
        createNewGameLabel(x, y);
    }

    private void exitIfGameWon() {
        if (matrix.isGameOver()) {
            JOptionPane.showMessageDialog(getRootPane(), "Congratulations! You won!");
            System.exit(0);
        }
    }

    private void setColorBorder(JLabel label, int i, int j) {
        label.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, THICKNESS));

        if (matrix.isCurrent() && i == matrix.getCurrentX() && j == matrix.getCurrentY()) {
            label.setBorder(BorderFactory.createLineBorder(Color.BLUE, THICKNESS));
        } else if (matrix.isNothingWasPressed()) {
            label.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, THICKNESS));
        }

        if (matrix.isSet() && i == matrix.getChosenX() && j == matrix.getChosenY()) {
            label.setBorder(BorderFactory.createLineBorder(chosenBoarder, THICKNESS));
        } else if (matrix.isNothingWasPressed()) {
            label.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, THICKNESS));
        }
    }

    private GridBagConstraints createCellConstraints(int i, int j) {
        return new GridBagConstraints(
                j,
                i,
                1,
                1,
                0,
                0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0),
                0,
                0
        );
    }

    @Override
    public void update(Observable o, Object arg) {
        updateCell(matrix.getPreChosenX(), matrix.getPreChosenY());
        updateCell(matrix.getPreCurrentX(), matrix.getPreCurrentY());
        updateCell(matrix.getCurrentX(), matrix.getCurrentY());
        updateCell(matrix.getChosenX(), matrix.getChosenY());
        revalidate();
        repaint();
    }

    private void updateCell(int x, int y) {
        exitIfGameWon();

        if (matrix.isDefinedCell(x, y)) {
            GridBagLayout layout = (GridBagLayout) this.getLayout();
            Component[] c = this.getComponents();
            for (Component comp : c) {
                if (comp instanceof JLabel) {
                    GridBagConstraints gbc = layout.getConstraints(comp);
                    if (gbc.gridx == y && gbc.gridy == x) {
                        remove(comp);
                        createNewGameLabel(x, y);
                    }
                }
            }
        }
    }

    private void createNewGameLabel(int x, int y) {
        String path = String.format(IMAGE_PATH_PATTERN, matrix.getMatrix()[x][y]);
        ImageIcon imageIcon = new ImageIcon(path);
        JLabel label = new JLabel(imageIcon);
        label.setPreferredSize(new Dimension(Demo14.FIELD_SIZE_IN_PIXEL, Demo14.FIELD_SIZE_IN_PIXEL));
        add(label, createCellConstraints(x, y));
        setColorBorder(label, x, y);

        gameMouseListener.initMouseListenerController(this, matrix, label, x, y);
    }

    public void subscribeOn(Observable o) {
        o.addObserver(this);
    }

    public Color getChosenBoarder() {
        return chosenBoarder;
    }

    public void setChosenBoarder(Color chosenBoarder) {
        this.chosenBoarder = chosenBoarder;
    }
}
