package net.javajoy.jps.homework.w14.puzzle.view;

import net.javajoy.jps.homework.w14.puzzle.controller.GameMouseListener;
import net.javajoy.jps.homework.w14.puzzle.controller.actions.*;
import net.javajoy.jps.homework.w14.puzzle.model.Matrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Kyselova Nataliia
 */

public class GameMenu extends JMenuBar {
    private Panel panel;
    private MainFrame frame;
    private Action newGameAction;
    private Action backgroundAction;
    private Action marqueeAction;
    private Action helpAction;
    private Action saveAction;
    private Action openAction;
    private JMenuBar menuBar;
    private JPopupMenu popupMenu;

    public JMenuBar initGameMainMenu(MainFrame frame, Panel panel, Matrix matrix, GameMouseListener gameMouseListener) {
        this.panel = panel;
        this.frame = frame;

        newGameAction = new NewGameAction(panel, matrix, gameMouseListener);
        backgroundAction = new ChooseColorAction("Choose a background color", "img/background.jpg", KeyEvent.VK_B,
                KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK), panel,
                Color.LIGHT_GRAY, Color.GREEN, Color.YELLOW, Color.WHITE, false);
        marqueeAction = new ChooseColorAction("Color selection marquee", "img/marquee.jpg", KeyEvent.VK_M,
                KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_DOWN_MASK), panel,
                Color.WHITE, Color.GREEN, Color.YELLOW, Color.RED, true);
        helpAction = new HelpAction(panel);
        saveAction = new SaveAction(panel);
        openAction = new OpenAction(panel);

        initJMenuBar();
        initJPopupMenu();
        initJToolBar();

        return menuBar;
    }

    private void initJToolBar() {
        JToolBar toolBar = new JToolBar();
        toolBar.add(newGameAction);
        toolBar.add(backgroundAction);
        toolBar.add(marqueeAction);
        toolBar.add(helpAction);
        toolBar.setFloatable(false);
        frame.getToolBarPanel().add(toolBar, BorderLayout.PAGE_START);
    }

    private void initJPopupMenu() {
        popupMenu = new JPopupMenu();
        popupMenu.add(newGameAction);
        popupMenu.add(backgroundAction);
        popupMenu.add(marqueeAction);
        popupMenu.setInvoker(panel);

        panel.add(popupMenu);
        panel.setComponentPopupMenu(popupMenu);
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    popupMenu.setLocation(e.getXOnScreen(), e.getYOnScreen());
                    popupMenu.setVisible(true);
                }
            }
        });
    }

    private void initJMenuBar() {
        menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("Menu");
        fileMenu.add(newGameAction);
        fileMenu.add(backgroundAction);
        fileMenu.add(marqueeAction);
        fileMenu.addSeparator();
        fileMenu.add(helpAction);
        menuBar.add(fileMenu);

        JMenu fileMenuFile = new JMenu("W14");
        fileMenuFile.add(saveAction);
        fileMenuFile.add(openAction);
        menuBar.add(fileMenuFile);
    }
}
