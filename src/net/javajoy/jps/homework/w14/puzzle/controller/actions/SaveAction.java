package net.javajoy.jps.homework.w14.puzzle.controller.actions;

import net.javajoy.jps.homework.w14.puzzle.Demo14;
import net.javajoy.jps.homework.w14.puzzle.model.Matrix;
import net.javajoy.jps.homework.w14.puzzle.view.Panel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Kyselova Nataliia
 */
public class SaveAction extends AbstractAction {
    public static final int WIDTH = Demo14.GRID_DIMENSION_HORIZONTAL;
    public static final int HEIGHT = Demo14.GRID_DIMENSION_VERTICAL;
    public static final String SAVE_PUZZLE_TXT = "savePuzzle.txt";

    private Panel panel;

    public SaveAction(Panel panel) {
        this.panel = panel;
        putValue(Action.NAME, "Save");
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(SAVE_PUZZLE_TXT))) {
            write(oos, Matrix.getInstance(WIDTH, HEIGHT));
            JOptionPane.showMessageDialog(panel, "Data have been saved");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void write(ObjectOutputStream oos, Matrix matrix) {
        try {
            oos.writeByte(WIDTH);
            oos.writeByte(HEIGHT);

            for (int i = 0; i < WIDTH; i++) {
                for (int j = 0; j < HEIGHT; j++) {
                    oos.writeInt(matrix.getElement(i, j));
                }
            }

            oos.writeInt(panel.getChosenBoarder().getRGB());
            oos.writeInt(panel.getBackground().getRGB());
            oos.flush();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
