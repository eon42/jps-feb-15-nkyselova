package net.javajoy.jps.homework.w14.puzzle.controller.actions;

import net.javajoy.jps.homework.w14.puzzle.controller.GameMouseListener;
import net.javajoy.jps.homework.w14.puzzle.model.Matrix;
import net.javajoy.jps.homework.w14.puzzle.view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * @author Kyselova Nataliia
 */
public class NewGameAction extends AbstractAction {
    private Panel panel;
    private Matrix matrix;
    private GameMouseListener gameMouseListener;

    public NewGameAction(final Panel panel, final Matrix matrix, final GameMouseListener gameMouseListener) {
        this.panel = panel;
        this.matrix = matrix;
        this.gameMouseListener = gameMouseListener;
        putValue(Action.NAME, "New game");
        putValue(Action.SMALL_ICON, new ImageIcon("img/new.jpg"));
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        initActionNew(panel, matrix, gameMouseListener);
    }

    private void initActionNew(net.javajoy.jps.homework.w14.puzzle.view.Panel panel, Matrix matrix, GameMouseListener gameMouseListener) {
        Color marqueeColor = panel.getChosenBoarder();
        panel.renewPanel(matrix, gameMouseListener, marqueeColor);
    }
}
