package net.javajoy.jps.homework.w12;

import net.javajoy.jps.homework.w12.controller.KeyboardListenerController;
import net.javajoy.jps.homework.w12.controller.MouseListenerController;
import net.javajoy.jps.homework.w12.model.Matrix;
import net.javajoy.jps.homework.w12.view.MainFrame;

/**
 * @author Kiselova Nataliia
 */
public class Demo12 {
    public static final int GRID_DIMENSION_VERTICAL = 3;
    public static final int GRID_DIMENSION_HORIZONTAL = 4;
    public static final int FIELD_SIZE_IN_PIXEL = 100;

    public static void main(String[] args) {
        //AN: инициализация правильная
        new MainFrame(Matrix.getInstance(GRID_DIMENSION_VERTICAL, GRID_DIMENSION_HORIZONTAL),
                new KeyboardListenerController(), new MouseListenerController());

    }

}
