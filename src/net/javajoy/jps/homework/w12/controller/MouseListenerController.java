package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.model.Matrix;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Kiselova Nataliia
 */
public class MouseListenerController extends MouseAdapter {

    public void initMouseListenerController(Matrix matrix, JLabel label, int x, int y) {

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                matrix.setPreCurrentX(matrix.getCurrentX());
                matrix.setPreCurrentY(matrix.getCurrentY());
                matrix.setCurrentX(x);
                matrix.setCurrentY(y);
                matrix.doOnClick();
                matrix.setPreCurrentX(matrix.getCurrentX());
                matrix.setPreCurrentY(matrix.getCurrentY());
                matrix.setNothingWasPressed(true);
                matrix.setCurrentX(Matrix.UNDEFINED_VALUE);
                matrix.setCurrentY(Matrix.UNDEFINED_VALUE);
            }
        };

        label.addMouseListener(mouseAdapter);
    }
}

