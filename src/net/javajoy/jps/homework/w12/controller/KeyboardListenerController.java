package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.model.Matrix;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Kiselova Nataliia
 */
public class KeyboardListenerController extends KeyAdapter {
    private Matrix matrix;

    public void initKeyboardListenerController(JFrame frame, Matrix matrix) {
        this.matrix = matrix;

        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_SPACE:
                        doOnSpace();
                        break;
                    case KeyEvent.VK_RIGHT:
                        doOnRight();
                        break;
                    case KeyEvent.VK_LEFT:
                        doOnLeft();
                        break;
                    case KeyEvent.VK_UP:
                        doOnUp();
                        break;
                    case KeyEvent.VK_DOWN:
                        doOnDown();
                        break;
                    default:
                        break;
                }
            }
        };

        frame.addKeyListener(keyAdapter);
    }

    private void doOnDown() {
        if (matrix.isNothingWasPressed()) {
            matrix.setFirstCell();
        } else {
            matrix.setPreCurrentX(matrix.getCurrentX());
            matrix.setPreCurrentY(matrix.getCurrentY());
            matrix.setCurrentX(matrix.getCurrentX() + 1 >= matrix.getWidth() ?
                    matrix.getCurrentX() : matrix.getCurrentX() + 1);
        }
    }

    private void doOnUp() {
        if (matrix.isNothingWasPressed()) {
            matrix.setFirstCell();
        } else {
            matrix.setPreCurrentX(matrix.getCurrentX());
            matrix.setPreCurrentY(matrix.getCurrentY());
            matrix.setCurrentX(matrix.getCurrentX() - 1 < 0 ?
                    matrix.getCurrentX() : matrix.getCurrentX() - 1);
        }
    }

    private void doOnLeft() {
        if (matrix.isNothingWasPressed()) {
            matrix.setFirstCell();
        } else {
            matrix.setPreCurrentX(matrix.getCurrentX());
            matrix.setPreCurrentY(matrix.getCurrentY());
            matrix.setCurrentY(matrix.getCurrentY() - 1 < 0 ?
                    matrix.getCurrentY() : matrix.getCurrentY() - 1);
        }
    }

    private void doOnRight() {
        if (matrix.isNothingWasPressed()) {
            matrix.setFirstCell();
        } else {
            matrix.setPreCurrentX(matrix.getCurrentX());
            matrix.setPreCurrentY(matrix.getCurrentY());
            matrix.setCurrentY(matrix.getCurrentY() + 1 >= matrix.getHeight() ?
                    matrix.getCurrentY() : matrix.getCurrentY() + 1);
        }
    }

    private void doOnSpace() {
        matrix.doOnClick();
    }
}
