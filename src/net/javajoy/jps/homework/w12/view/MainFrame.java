package net.javajoy.jps.homework.w12.view;

import net.javajoy.jps.homework.w12.Demo12;
import net.javajoy.jps.homework.w12.controller.KeyboardListenerController;
import net.javajoy.jps.homework.w12.controller.MouseListenerController;
import net.javajoy.jps.homework.w12.model.Matrix;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kiselova Nataliia
 */
public class MainFrame extends JFrame {

    public static final int PADDING_IN_PIXELS = 1;
    //  private final int gridDimensionVertical; //AN: поля не нужны
    // private final int gridDimensionHorizontal;
    private final int fieldSize;
//    private Matrix matrix;

    public MainFrame(Matrix matrix, KeyboardListenerController keyboardListenerController,
                     MouseListenerController mouseListenerController) {
        //   this.gridDimensionVertical = matrix.getHeight();
        // this.gridDimensionHorizontal = matrix.getWidth();
        this.fieldSize = Demo12.FIELD_SIZE_IN_PIXEL;
        //this.matrix = matrix;
        initFrame(mouseListenerController, matrix);
        keyboardListenerController.initKeyboardListenerController(this, matrix);
    }

    private void initFrame(MouseListenerController mouseListenerController, Matrix matrix) {
        Panel panel = new Panel(matrix, mouseListenerController);
        setPreferredSize(
                new Dimension(
                        (matrix.getHeight() + PADDING_IN_PIXELS) * fieldSize,
                        (matrix.getWidth() + PADDING_IN_PIXELS) * fieldSize
                )
        );
        //KN: добавляю дополнительный размер для поля, чтобы влезли все клеточки с полями и был отступ от края поля
        //AN: лучше назвать тогда как константу
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Puzzle");
        setResizable(false);
        setContentPane(panel);
        pack();
        setLocationRelativeTo(null);
        showWelcomeDialog(panel);
        setVisible(true);
    }

    private void showWelcomeDialog(Panel panel) {
        JOptionPane.showMessageDialog(panel.getRootPane(), "How to control:\n" +
                "Mouse: choose the picture and choose another to swap\n" +
                "Keyboard: click \"Left\", \"Right\", \"Up\", \"Down\" to move on pictures \n" +
                "and \"Space\" to choose the picture and choose another to swap");
    }
}
