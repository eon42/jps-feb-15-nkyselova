package net.javajoy.jps.homework.w12.view;

import net.javajoy.jps.homework.w12.Demo12;
import net.javajoy.jps.homework.w12.controller.MouseListenerController;
import net.javajoy.jps.homework.w12.model.Matrix;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Kiselova Nataliia
 */
public class Panel extends JPanel implements Observer {
    public static final int THICKNESS = 3;
    public static final String IMAGE_PATH_PATTERN = "img/%d.jpg";

    private Matrix matrix;
    private MouseListenerController mouseListenerController;

    public Panel(Matrix matrix, MouseListenerController mouseListenerController) {
        this.matrix = matrix;
        this.mouseListenerController = mouseListenerController;

        initFrame();
        matrix.shuffle();
        initFields(mouseListenerController);
        subscribeOn(matrix);
    }

    private void initFrame() {
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(matrix.getHeight() * Demo12.FIELD_SIZE_IN_PIXEL,
                matrix.getWidth() * Demo12.FIELD_SIZE_IN_PIXEL));
        setMinimumSize(new Dimension(matrix.getHeight() * Demo12.FIELD_SIZE_IN_PIXEL,
                matrix.getWidth() * Demo12.FIELD_SIZE_IN_PIXEL));
    }

    private void initFields(MouseListenerController mouseListenerController) {
        for (int i = 0; i < matrix.getWidth(); i++) {
            for (int j = 0; j < matrix.getHeight(); j++) {
                addCell(i, j, mouseListenerController);
            }
        }
    }

    private void addCell(int x, int y, MouseListenerController mouseListenerController) {
        exitIfGameWon(); //AN: немного странновато  что выход из программы может быть из метода addCell
        createNewGameLabel(x, y); //AN: вынес в метод, так как тот же код был еще в методе updateCell
    }

    private void exitIfGameWon() {
        if (matrix.isGameOver()) {
            JOptionPane.showMessageDialog(getRootPane(), "Congratulations! You won!");
            System.exit(0);
        }
    }

    private void setColorBoarder(JLabel label, int i, int j) {
        label.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, THICKNESS));

        if (matrix.isCurrent() && i == matrix.getCurrentX() && j == matrix.getCurrentY()) {
            label.setBorder(BorderFactory.createLineBorder(Color.BLUE, THICKNESS));
        } else if (matrix.isNothingWasPressed()) {
            label.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, THICKNESS));
        }

        if (matrix.isSet() && i == matrix.getChosenX() && j == matrix.getChosenY()) {
            label.setBorder(BorderFactory.createLineBorder(Color.RED, THICKNESS));
        } else if (matrix.isNothingWasPressed()) {
            label.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, THICKNESS));
        }
    }

    private GridBagConstraints createCellConstraints(int i, int j) {
        return new GridBagConstraints(
                j,
                i,
                1,
                1,
                0,
                0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0),
                0,
                0
        );
    }

    @Override
    public void update(Observable o, Object arg) {
        updateCell(matrix.getPreChosenX(), matrix.getPreChosenY());
        updateCell(matrix.getPreCurrentX(), matrix.getPreCurrentY());
        updateCell(matrix.getCurrentX(), matrix.getCurrentY());
        updateCell(matrix.getChosenX(), matrix.getChosenY());
        revalidate();
        repaint();
    }

    private void updateCell(int x, int y) {
        exitIfGameWon();

        if (x != Matrix.UNDEFINED_VALUE && y != Matrix.UNDEFINED_VALUE) {
            GridBagLayout layout = (GridBagLayout) this.getLayout();
            Component[] c = this.getComponents();
            for (Component comp : c) {
                if (comp instanceof JLabel) {
                    GridBagConstraints gbc = layout.getConstraints(comp);
                    if (gbc.gridx == y && gbc.gridy == x) {
                        remove(comp);
                        createNewGameLabel(x, y);
                    }
                }
            }
        }
    }

    private void createNewGameLabel(int x, int y) {
        String path = String.format(IMAGE_PATH_PATTERN, matrix.getMatrix()[x][y]);
        ImageIcon imageIcon = new ImageIcon(path);
        JLabel label = new JLabel(imageIcon);
        label.setPreferredSize(new Dimension(Demo12.FIELD_SIZE_IN_PIXEL, Demo12.FIELD_SIZE_IN_PIXEL));
        add(label, createCellConstraints(x, y));
        setColorBoarder(label, x, y);

        mouseListenerController.initMouseListenerController(matrix, label, x, y);
    }

    public void subscribeOn(Observable o) {
        o.addObserver(this);
    }
}
