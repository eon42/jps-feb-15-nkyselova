package net.javajoy.jps.homework.w03;

/*
  Продемонстрируйте создание нескольких экземпляров для каждого из классов и вывод их состояния на печать.
*/

public class Main {

    public static void main(String[] args) {

        Time timeFirst = new Time(7, 0, 56);
        Time timeSecond = new Time(0, 18, 23);
        Time timeThird = new Time(17, 20, 0);

        DayPart dayPartOne = new DayPart(timeFirst);
        DayPart dayPartTwo = new DayPart(timeSecond);
        DayPart dayPartThree = new DayPart(timeThird);

        checkTime(timeFirst, dayPartOne);
        checkTime(timeSecond, dayPartTwo);
        checkTime(timeThird, dayPartThree);
    }

    private static void checkTime(Time time, DayPart dayPart) {
        System.out.println(time);
        System.out.println(dayPart.defineDayPart());
        System.out.println(dayPart + "\n");
    }
}
