package net.javajoy.jps.homework.w03;

/*
  Создайте производный от него класс, в котором:
  - добавьте метод определения времени дня.
    Результатом такого метода должен являться экземпляр перечисления вида {«утро»,«день»,«вечер»,«ночь»};
  - переопределите метод toString() для вывода времени в текстовом виде, например
  «десять часов : двадцать пять минут» вместо «10:25». По желанию можете добавить также вывод секунд.
 */

public class DayPart extends Time {

    /**
     * Copying constructor. Initialise DayPart instance by copying Time instance.
     *
     * @param time
     */
    public DayPart(Time time) {
        super(time);
    }

    /**
     * Define what part of day when time is timeInstance
     *
     * @return enum dayTime
     */
    public DayTimes defineDayPart() {
        DayTimes dayTime;

        if (this.getHour() >= DayTimes.EVENING.beginHour()) {
            dayTime = DayTimes.EVENING;
        } else {
            if (this.getHour() >= DayTimes.DAY.beginHour()) {
                dayTime = DayTimes.DAY;
            } else {
                if (this.getHour() >= DayTimes.MORNING.beginHour()) {
                    dayTime = DayTimes.MORNING;
                } else {
                    dayTime = DayTimes.NIGHT;
                }
            }
        }
        return dayTime;
    }

    /**
     * {@inheritdoc}
     */
    @Override
    public String toString() {
        return String.format("%s%s : %s%s : %s%s", getDigitsInLetters(this.getHour(), true),
                getDayPartInstanceInLetters(this.getHour(), 0),
                getDigitsInLetters(this.getMin(), false),
                getDayPartInstanceInLetters(this.getMin(), 1),
                getDigitsInLetters(this.getSec(), false),
                getDayPartInstanceInLetters(this.getSec(), 2));
    }

    /**
     * Define correct form of "час/минута/секунда" depending on amount of time
     *
     * @param timeUnit, i
     * @return string
     */

    private String getDayPartInstanceInLetters(int timeUnit, int i) {
        String word = "";
        if (timeUnit % 10 == 1 && timeUnit != 11) {
            if (i == 0) {
                word = "час";
            } else if (i == 1) {
                word = "минута";
            } else if (i == 2) {
                word = "секунда";
            }
        } else if (timeUnit % 10 >= 2 && timeUnit % 10 <= 4 &&
                timeUnit != 12 && timeUnit != 13 && timeUnit != 14) {
            if (i == 0) {
                word = "часа";
            } else if (i == 1) {
                word = "минуты";
            } else if (i == 2) {
                word = "секунды";
            }
        } else {
            if (i == 0) {
                word = "часов";
            } else if (i == 1) {
                word = "минут";
            } else if (i == 2) {
                word = "секунд";
            }
        }

        return word;
    }

    /**
     * Translate digits in letters
     *
     * @param digit
     * @param isMale
     * @return string
     */
    private String getDigitsInLetters(int digit, boolean isMale) {
        String digitStringFirst = "", digitString = "";
        if (digit >= 20 || digit == 0) {
            switch ((digit / 10)) {
                case 0:
                    digitStringFirst = "ноль ";
                    break;
                case 2:
                    digitStringFirst = "двадцать ";
                    break;
                case 3:
                    digitStringFirst = "тридцать ";
                    break;
                case 4:
                    digitStringFirst = "сорок ";
                    break;
                case 5:
                    digitStringFirst = "пятьдесят ";
                    break;
            }
        }

        if (digit >= 20) {
            digit = digit % 10;
        }

        switch (digit) {
            case 1:
                if (isMale) {
                    digitString = "один ";
                } else {
                    digitString = "одна ";
                }
                break;
            case 2:
                if (isMale) {
                    digitString = "два ";
                } else {
                    digitString = "две ";
                }
                break;
            case 3:
                digitString = "три ";
                break;
            case 4:
                digitString = "четыре ";
                break;
            case 5:
                digitString = "пять ";
                break;
            case 6:
                digitString = "шесть ";
                break;
            case 7:
                digitString = "семь ";
                break;
            case 8:
                digitString = "восемь ";
                break;
            case 9:
                digitString = "девять ";
                break;
            case 10:
                digitString = "десять ";
                break;
            case 11:
                digitString = "одиннадцать ";
                break;
            case 12:
                digitString = "двенадцать ";
                break;
            case 13:
                digitString = "тринадцать ";
                break;
            case 14:
                digitString = "четырнадцать ";
                break;
            case 15:
                digitString = "пятнадцать ";
                break;
            case 16:
                digitString = "шестнадцать ";
                break;
            case 17:
                digitString = "семнадцать ";
                break;
            case 18:
                digitString = "восемнадцать ";
                break;
            case 19:
                digitString = "девятнадцать ";
                break;
        }

        return digitStringFirst + digitString;
    }


}
