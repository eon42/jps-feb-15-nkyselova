package net.javajoy.jps.homework.w03;

/*
  1. Напишите класс «время» для представления времени в пределах суток. Внутренне представление  - три целые числа,
     задающие часы, минуты и секунды.  Класс должен содержать:
  -  конструкторы (по умолчанию, с параметрами и копирующий);
  -  статический метод создания объекта по трем заданным числам;
  -  статический метод сравнения на больше-меньше;
  -  не статический метод нахождения секунд, разделяющих два времени.

  w03 : В классе "Время" из ДЗ 02, переопределите метод toString() для удобного вывода состояния объекта на печать.
 */

public class Time {

    public static final int SEC_IN_MIN = 60;
    public static final int MIN_IN_HOUR = 60;

    private int hour;
    private int min;
    private int sec;

    /**
     * Initialise Time instance by default.
     */
    public Time() {
    }

    /**
     * Initialise Time instance using parameters.
     *
     * @param hour
     * @param min
     * @param sec
     */
    public Time(int hour, int min, int sec) {
        this.hour = hour;
        this.min = min;
        this.sec = sec;
    }

    /**
     * Copying constructor. Initialise Time instance by copying.
     *
     * @param time
     */
    public Time(Time time) {
        this.hour = time.hour;
        this.min = time.min;
        this.sec = time.sec;
    }

    /**
     * Set hour in Time instance
     *
     * @param hour
     */
    public void setHour(int hour) {
        this.hour = hour;
    }

    /**
     * Set minutes in Time instance
     *
     * @param min
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * Set seconds in Time instance
     *
     * @param sec
     */
    public void setSec(int sec) {
        this.sec = sec;
    }

    /**
     * Get hour from Time instance
     *
     * @return int hour
     */
    public int getHour() {
        return hour;
    }

    /**
     * Get minutes from Time instance
     *
     * @return int min
     */
    public int getMin() {
        return min;
    }

    /**
     * Get seconds from Time instance
     *
     * @return int sec
     */
    public int getSec() {
        return sec;
    }

    /**
     * Static method creating Time instance
     *
     * @param hour
     * @param min
     * @param sec
     * @return Time instance
     */
    public static Time create(int hour, int min, int sec) {
        return new Time(hour, min, sec);
    }

    /**
     * Compare two Time instances
     *
     * @param timeFirst
     * @param timeSecond
     * @return boolean true if first param larger then second
     */
    public static boolean compare(Time timeFirst, Time timeSecond) {
        return timeFirst.convertTimeToSec() >= timeSecond.convertTimeToSec();
    }

    /**
     * Count difference in seconds between instance, which call the method and a param
     * (positive if instance larger then param)
     *
     * @param time
     * @return int difference in seconds
     */
    public int countDifferenceInSeconds(Time time) {
        return this.convertTimeToSec() - time.convertTimeToSec();
    }

    /**
     * Count time in seconds
     *
     * @return int time in seconds
     */
    private int convertTimeToSec() {
        return this.hour * MIN_IN_HOUR * SEC_IN_MIN + this.min * SEC_IN_MIN + this.sec;
    }

    /**
     * {@inheritdoc}
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", this.getHour(), this.getMin(), this.getSec());
    }
}
