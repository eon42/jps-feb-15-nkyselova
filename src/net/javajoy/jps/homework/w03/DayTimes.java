package net.javajoy.jps.homework.w03;

/*
  В перечислении («утро», «день», «вечер», «ночь») сохраните время начала и конца каждого периода.
  Реализуйте в этом перечислении конструктор и методы для соответствующих полей.
 */

public enum DayTimes {
    MORNING(6, 0, 0, 11, 59, 59),
    DAY(12, 0, 0, 17, 59, 59),
    EVENING(18, 0, 0, 23, 59, 59),
    NIGHT(0, 0, 0, 5, 59, 59);

    private int beginHour, beginSec, beginMinute, endHour, endMinute, endSec; //AN: желательно не объявлять поля в одну линию

    /**
     * Initialise enumeration
     *
     * @param beginHour
     * @param beginMinute
     * @param beginSec
     * @param endHour
     * @param endMinute
     * @param endSec
     */
    DayTimes(int beginHour, int beginMinute, int beginSec, int endHour, int endMinute, int endSec) {
        this.beginHour = beginHour;
        this.beginMinute = beginMinute;
        this.beginSec = beginSec;
        this.endHour = endHour;
        this.endMinute = endMinute;
        this.endSec = endSec;
    }

    /**
     * Return value of hour when DayTimes is begin
     *
     * @return beginHour
     */
    int beginHour(){
        return beginHour;
    }

    /**
     * Return value of minute when DayTimes is begin
     *
     * @return beginMinute
     */
    int beginMinute(){
        return beginMinute;
    }

    /**
     * Return value of second when DayTimes is begin
     *
     * @return beginSec
     */
    int beginSec(){
        return beginSec;
    }

    /**
     * Return value of hour when DayTimes is end
     *
     * @return endHour
     */
    int endHour(){
        return endHour;
    }

    /**
     * Return value of minute when DayTimes is end
     *
     * @return endMinute
     */
    int endMinute(){
        return endMinute;
    }

    /**
     * Return value of second when DayTimes is end
     *
     * @return endSec
     */
    int endSec(){
        return endSec;
    }
}
