package net.javajoy.jps.homework.w16;

import net.javajoy.jps.homework.w16.controller.MainController;

/**
 * @author Kyselova Nataliia
 */
public class Demo16 {

    public static void main(String[] args) {
        new MainController();
    }
}
