package net.javajoy.jps.homework.w16.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class ContactBook {
    private List<String> friends;
    private List<String> acquaintances;

    public ContactBook() {
        initFriends();
        initAcquaintances();
    }

    private void initFriends() {
        friends = new ArrayList<>();
        friends.add("Anna");
        friends.add("Alexandr");
        friends.add("Andrey");
        friends.add("Alexei");
        friends.add("Artur");
        friends.add("Anastasia");
        friends.add("Alina");
    }

    private void initAcquaintances() {
        acquaintances = new ArrayList<>();
        acquaintances.add("Peter");
        acquaintances.add("Vasiliy");
        acquaintances.add("Ipollit");
        acquaintances.add("Ivan");
        acquaintances.add("Veniamin");
        acquaintances.add("Eduard");
    }

    public List<String> getFriends() {
        return friends;
    }

    public List<String> getAcquaintances() {
        return acquaintances;
    }
}
