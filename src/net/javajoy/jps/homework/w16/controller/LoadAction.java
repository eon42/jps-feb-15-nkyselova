package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.model.ContactBook;
import net.javajoy.jps.homework.w16.view.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class LoadAction implements ActionListener {
    private ContactBook contacts;
    private MainPanel panel;

    public LoadAction(ContactBook contacts, MainPanel panel) {
        this.contacts = contacts;
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("save.txt"))) {
            refreshContacts(ois, contacts.getFriends());
            refreshContacts(ois, contacts.getAcquaintances());
            JOptionPane.showMessageDialog(panel, "Data have been loaded");
            panel.refreshLists();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void refreshContacts(ObjectInputStream ois, List<String> list) {
        if (ois != null) {
            list.clear();
            try {
                int size = ois.readInt();
                for (int i = 0; i < size; i++) {
                    list.add(ois.readUTF());
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
