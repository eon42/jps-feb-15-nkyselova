package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.view.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class ChangeListListener implements ActionListener {

    private List<String> toDelete;
    private List<String> toAdd;
    private JList<String> list;
    private MainPanel panel;

    public ChangeListListener(List<String> toDelete, List<String> toAdd,
                              JList<String> list, MainPanel panel) {
        this.toDelete = toDelete;
        this.toAdd = toAdd;
        this.list = list;
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (list.getSelectedIndex() < toDelete.size() && list.getSelectedValue() != null) {
            int selectedIndex = list.getSelectedIndex();
            String name = list.getSelectedValue();
            toAdd.add(name);
            toDelete.remove(name);
            panel.refreshLists();

            if (list.getSelectedIndex() < toDelete.size()) {
                list.setSelectedIndex(selectedIndex);
            }
        }
    }
}
