package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.model.ContactBook;
import net.javajoy.jps.homework.w16.view.MainFrame;
import net.javajoy.jps.homework.w16.view.MainMenu;
import net.javajoy.jps.homework.w16.view.MainPanel;

/**
 * @author Kyselova Nataliia
 */
public class MainController {

    public MainController() {
        ContactBook contacts = new ContactBook();

        MainFrame frame = new MainFrame(contacts);
        MainPanel panel = (MainPanel) frame.getContentPane();
        MainMenu menu = frame.getMainMenu();

        panel.getToRight().addActionListener(new ChangeListListener(contacts.getAcquaintances(),
                contacts.getFriends(), panel.getAcquaintancesList(), panel));
        panel.getToLeft().addActionListener(new ChangeListListener(contacts.getFriends(),
                contacts.getAcquaintances(), panel.getFriendsList(), panel));

        menu.getSaveMenu().addActionListener(new SaveAction(contacts, panel));
        menu.getLoadMenu().addActionListener(new LoadAction(contacts, panel));
    }
}
