package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.model.ContactBook;
import net.javajoy.jps.homework.w16.view.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class SaveAction implements ActionListener {
    private ContactBook contacts;
    private MainPanel panel;

    public SaveAction(ContactBook contacts, MainPanel panel) {
        this.contacts = contacts;
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("save.txt"))) {
            write(oos, contacts.getFriends());
            write(oos, contacts.getAcquaintances());
            JOptionPane.showMessageDialog(panel, "Data have been saved");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void write(ObjectOutputStream oos, List<String> list) {
        if (oos != null) {
            try {
                oos.writeInt(list.size());
                for (String s : list) {
                    oos.writeUTF(s);
                }
                oos.flush();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
