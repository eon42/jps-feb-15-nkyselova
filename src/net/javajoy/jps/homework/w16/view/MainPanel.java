package net.javajoy.jps.homework.w16.view;

import net.javajoy.jps.homework.w16.model.ContactBook;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class MainPanel extends JPanel {
    private ContactBook contacts;
    private JList<String> acquaintancesList;
    private JList<String> friendsList;
    private JButton toRight;
    private JButton toLeft;

    public MainPanel(ContactBook contacts) {
        this.contacts = contacts;

        setLayout(new GridBagLayout());

        JLabel acquaintances = new JLabel("Acquaintances");
        add(acquaintances, createCellConstraints(0, 0, 1, 1, 0, 1));

        JLabel friends = new JLabel("Friends");
        add(friends, createCellConstraints(2, 0, 1, 1, 0, 1));

        acquaintancesList = createList(contacts.getAcquaintances());
        add(new JScrollPane(this.acquaintancesList), createCellConstraints(0, 1, 1, 2, 0, 1));

        toRight = new JButton("==>");
        add(toRight, createCellConstraints(1, 1, 1, 1, 1, 1));

        toLeft = new JButton("<==");
        add(toLeft, createCellConstraints(1, 2, 1, 1, 1, 1));

        friendsList = createList(contacts.getFriends());
        add(new JScrollPane(this.friendsList), createCellConstraints(2, 1, 1, 2, 0, 1));
    }

    private JList<String> createList(List<String> contacts) {
        JList<String> list = new JList<>();
        list.setListData(getStringArray(contacts));
        list.setVisibleRowCount(3);
        list.setFixedCellHeight(50);
        list.setFixedCellWidth(150);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        return list;
    }

    private GridBagConstraints createCellConstraints(int gridx, int gridy, int gridwidth, int gridheight,
                                                     int weightx, int weighty) {
        return new GridBagConstraints(
                gridx,
                gridy,
                gridwidth,
                gridheight,
                weightx,
                weighty,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0),
                0,
                0
        );
    }

    public JList<String> getAcquaintancesList() {
        return acquaintancesList;
    }

    public JList<String> getFriendsList() {
        return friendsList;
    }

    public JButton getToRight() {
        return toRight;
    }

    public JButton getToLeft() {
        return toLeft;
    }

    public void refreshLists() {
        getAcquaintancesList().setListData(getStringArray(contacts.getAcquaintances()));
        getAcquaintancesList().repaint();
        getAcquaintancesList().revalidate();
        getFriendsList().setListData(getStringArray(contacts.getFriends()));
        getFriendsList().repaint();
        getFriendsList().revalidate();
    }

    public String[] getStringArray(List<String> contacts) {
        return contacts.toArray(new String[contacts.size()]);
    }
}
