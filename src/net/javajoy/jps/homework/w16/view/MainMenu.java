package net.javajoy.jps.homework.w16.view;

import javax.swing.*;

/**
 * @author Kyselova Nataliia
 */
public class MainMenu extends JMenuBar {
    private JMenuItem saveMenu;
    private JMenuItem loadMenu;

    public MainMenu() {
        initMenuBar();
    }

    public void initMenuBar() {
        JMenu fileMenu = new JMenu("Menu");
        saveMenu = new JMenuItem("Save");
        loadMenu = new JMenuItem("Load");
        fileMenu.add(saveMenu);
        fileMenu.add(loadMenu);
        add(fileMenu);
    }

    public JMenuItem getSaveMenu() {
        return saveMenu;
    }

    public JMenuItem getLoadMenu() {
        return loadMenu;
    }
}
