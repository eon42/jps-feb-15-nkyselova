package net.javajoy.jps.homework.w16.view;

import net.javajoy.jps.homework.w16.model.ContactBook;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class MainFrame extends JFrame {

    private MainMenu mainMenu;

    public MainFrame(ContactBook contacts) {
        initFrame(contacts);
    }

    private void initFrame(ContactBook contacts) {
        setPreferredSize(new Dimension(500, 300));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("ContactList");
        setResizable(false);
        setContentPane(new MainPanel(contacts));
        mainMenu = new MainMenu();
        setJMenuBar(mainMenu);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }
}
