package net.javajoy.jps.homework.w10;

/**
 * @author Kiselova Nataliia
 */
public class Demo10 {
    public static void main(String[] args) {
        DataBaseLoginPassword data = new DataBaseLoginPassword();
        data = data.fillData();
        new AuthorizationForm(data);
    }
}
