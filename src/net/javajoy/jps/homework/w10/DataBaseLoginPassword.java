package net.javajoy.jps.homework.w10;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kiselova Nataliia
 */
public class DataBaseLoginPassword {

    private Map<String, String> storage = new HashMap<>();

    /**
     * Check if key ang login is in map
     *
     * @param key
     * @param login
     * @return boolean
     */
    public boolean isCorrect(String key, String login) {
        for (Map.Entry<String, String> entry : storage.entrySet()) {
            if (key.equals(entry.getKey()) && login.equals(entry.getValue())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Fill map
     *
     * @return map
     */
    public DataBaseLoginPassword fillData() {
        this.storage.put("anna", "12345");
        this.storage.put("andrey", "aaa1");
        this.storage.put("alexander", "a@a11");
        this.storage.put("elena", "12345");
        return this;
    }
}
