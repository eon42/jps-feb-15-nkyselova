package net.javajoy.jps.homework.w07;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kiselova Nataliia
 */
public class Matrix {
    private List<List<Integer>> data = new ArrayList<>();
    private int rowSize;
    private int columnSize;

    /**
     * Constructor
     */
    public Matrix() {

    }

    /**
     * Constructor with parameters. Fill matrix random integer digits.
     *
     * @param rowSize    quantity rows in matrix
     * @param columnSize quantity column in matrix
     */
    public Matrix(int rowSize, int columnSize, boolean isFillRandom) {
        if (isFillRandom) {
            fillMatrixRandom(rowSize, columnSize);
        } else {
            this.rowSize = rowSize;
            this.columnSize = columnSize;
        }
    }

    /**
     * Create and fill matrix random integer digits
     *
     * @param rowSize    quantity rows in matrix
     * @param columnSize quantity column in matrix
     * @return object Matrix
     */
    public Matrix fillMatrixRandom(int rowSize, int columnSize) {
        this.rowSize = rowSize;
        this.columnSize = columnSize;
        for (int i = 0; i < rowSize; i++) {
            List<Integer> column = new ArrayList<>();
            for (int j = 0; j < columnSize; j++) {
                column.add((int) (Math.random() * 10));
            }
            this.data.add(column);
        }
        return this;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return matrixToString();
    }

    /**
     * Write matrix in string
     *
     * @return String s
     */
    public String matrixToString() {
        String s = "";
        for (int i = 0; i < this.rowSize; i++) {
            for (int j = 0; j < this.columnSize; j++) {
                s = s.concat(this.data.get(i).get(j) + " ");
            }
            s = s.concat("\n");
        }
        return s;
    }

    /**
     * Multiply two matrices. If it is not impossible - returns null.
     *
     * @param matrix second matrix that should be multiplied
     * @return matrix - the result of multiplication two matrices
     */
    public Matrix multiply(Matrix matrix) {
        if (this.columnSize != matrix.rowSize) {
            return null;
        }

        Matrix resultMatrix = new Matrix(this.rowSize, matrix.columnSize, false);
        int sum = 0;
        for (int i = 0; i < this.rowSize; i++) {
            List<Integer> column = new ArrayList<>();
            for (int j = 0; j < matrix.columnSize; j++) {
                for (int k = 0; k < matrix.rowSize; k++) {
                    sum += this.data.get(i).get(k) * matrix.data.get(k).get(j);
                }
                column.add(sum);
                sum = 0;
            }
            resultMatrix.data.add(column);
        }
        return resultMatrix;
    }
}
