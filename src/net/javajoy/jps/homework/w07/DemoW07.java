package net.javajoy.jps.homework.w07;

import net.javajoy.jps.homework.w06.util.StringUtils;
import net.javajoy.jps.homework.w06.collection.tree.StringTree;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Kiselova Nataliia
 */

public class DemoW07 {
    public static void main(String[] args) throws Exception {
        testStack("}if(a>b()) f(s){;");
        testStack("if(a>b()) f(s);");
        testTreeString();
        testMatrix();
    }

    private static void testMatrix() {
        Matrix matrixA = new Matrix(1, 2, true);
        System.out.println(matrixA);

        Matrix matrixB = new Matrix(2, 3, true);
        System.out.println(matrixB);


        System.out.println(matrixA.multiply(matrixB)); //AN: сразу печатаем рез-

        System.out.println("******************************************************************************");
    }

    private static void testStack(String input) throws Exception {
        System.out.println("String, which is check:" + input);
        Deque<String> stack = new ArrayDeque<>(); //AN: используем сразу стандартный класс
        //KN: Не правильно сразу тебя поняла в прошлый раз. Думала, что ArrayDeque<> нужно использовать только
        //KN: при нерекурсивных поисках,
        //KN: поэтому в задаче про проверку расставления скобок ничего не изменила. Но теперь понятно.

        for (char c : input.toCharArray()) {
            if (StringUtils.isBracket(c)) {
                if (StringUtils.isPairBracket(String.valueOf(c), String.valueOf(stack.peek()))) {
                    stack.pop();
                } else {
                    stack.push(String.valueOf(c));
                }
            }
        }

        System.out.printf("Stack content is %s, result: %s%n", stack, stack.isEmpty() ? "OK" : "WRONG");

        System.out.println("******************************************************************************");
    }

    private static void testTreeString() throws Exception {
        StringTree root = new StringTree("6");

        StringTree left = new StringTree("4");
        left.setLeft(new StringTree("3"));
        left.setRight(new StringTree("5"));
        root.setLeft(left);

        StringTree right = new StringTree("8");
        right.setLeft(new StringTree("7"));
        right.setRight(new StringTree("10"));
        root.setRight(right);

        /*
                6
            4       8
          3   5   7  10

         */

        System.out.println("\nNon-recursive depth-first search using stack (DFS)");
        dfsByStack(root);

        System.out.println("\n\nNon-recursive breadth-first search using stack (DFS)");
        bfsByQueue(root);
        System.out.println("\n******************************************************************************");

    }

    private static void dfsByStack(StringTree stringTree) throws Exception {
        Deque<StringTree> stack = new ArrayDeque<>();
        stack.push(stringTree);

        while (!stack.isEmpty()) {
            if (!stack.isEmpty()) {
                stringTree = stack.pop();
                System.out.print(stringTree.getValue() + " ");
            }
            while (stringTree != null) {
                if (stringTree.getRight() != null) {
                    stack.push(stringTree.getRight());
                }
                stringTree = stringTree.getLeft();
                if (stringTree != null) {
                    System.out.print(stringTree.getValue() + " ");
                }
            }
        }


    }

    private static void bfsByQueue(StringTree stringTree) throws Exception {
        Queue<StringTree> queue = new LinkedList<>();
        queue.add(stringTree);

        while (!queue.isEmpty()) {
            StringTree node = queue.poll();
            System.out.print(node.getValue() + " ");

            if (node.getLeft() != null) {
                queue.add(node.getLeft());
            }

            if (node.getRight() != null) {
                queue.add(node.getRight());
            }
        }
    }
}
