package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.model.GoodTableModel;
import net.javajoy.jps.homework.w17.view.MainFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Kyselova Nataliia
 */
public class SaveController implements ActionListener {
    public static final String SAVE_GOOD_TXT = "saveGood.txt";
    private MainFrame frame;

    public SaveController(MainFrame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(SAVE_GOOD_TXT))) {
            oos.writeObject(GoodTableModel.getInstance().getList());
            JOptionPane.showMessageDialog(frame.getContentPane(), "Data have been saved");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
