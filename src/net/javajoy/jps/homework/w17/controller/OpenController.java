package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.model.Good;
import net.javajoy.jps.homework.w17.model.GoodTableModel;
import net.javajoy.jps.homework.w17.view.MainFrame;
import net.javajoy.jps.homework.w17.view.MainPanel;
import net.javajoy.jps.homework.w17.view.renderer.CellRenderer;

import javax.swing.*;
import javax.swing.table.TableColumnModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class OpenController implements ActionListener {
    private MainFrame frame;
    private GoodTableModel goodTableModel;

    public OpenController(MainFrame frame, GoodTableModel goodTableModel) {
        this.frame = frame;
        this.goodTableModel = goodTableModel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        File file = new File(SaveController.SAVE_GOOD_TXT);

        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                Object listGoods = ois.readObject();
                goodTableModel.setData(((List<Good>) listGoods));

                TableColumnModel columnModel = ((MainPanel) frame.getContentPane()).getTable().getColumnModel();
                columnModel.getColumn(GoodTableModel.COLOR_COLUMN).setCellRenderer(new CellRenderer());
                columnModel.getColumn(GoodTableModel.IMG_COLUMN).setCellRenderer(new CellRenderer());

                JOptionPane.showMessageDialog(frame.getContentPane(), "Data have been loaded");
            } catch (ClassNotFoundException | IOException ex) {
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(frame.getContentPane(), "There is no saved price");
        }
    }
}
