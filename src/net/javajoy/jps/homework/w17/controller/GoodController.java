package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.model.GoodTableModel;
import net.javajoy.jps.homework.w17.view.MainFrame;
import net.javajoy.jps.homework.w17.view.MainPanel;
import net.javajoy.jps.homework.w17.view.add.AddGoodsFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Kyselova Nataliia
 */
public class GoodController {
    private JTable table;
    private GoodTableModel goodTableModel;
    private MainPanel panel;
    private AddGoodsFrame frame;

    private static GoodController instance;

    private GoodController() {
    }

    public ActionListener createAddGoodListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame = new AddGoodsFrame();
                new AddController(panel, frame);
            }
        };
    }

    public ActionListener createDeleteGoodListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int selectedRow : table.getSelectedRows()) {
                    goodTableModel.removeRow(selectedRow);
                }
            }
        };
    }

    public static GoodController get() {
        if (instance == null)
            instance = new GoodController();
        return instance;
    }

    public void setFrame(MainFrame frame) {
        this.panel = (MainPanel) frame.getContentPane();
        this.table = panel.getTable();
        this.goodTableModel = (GoodTableModel) panel.getTable().getModel();
    }

    public AddGoodsFrame getGoodsFrame() {
        return frame;
    }
}