package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.model.Good;
import net.javajoy.jps.homework.w17.model.GoodTableModel;
import net.javajoy.jps.homework.w17.view.MainPanel;
import net.javajoy.jps.homework.w17.view.add.AddGoodsFrame;
import net.javajoy.jps.homework.w17.view.add.AddGoodsPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author Kyselova Nataliia
 */
public class AddController {
    private AddGoodsPanel addGoodsPanel;
    private MainPanel panel;
    private GoodTableModel goodTableModel;

    private Color chosenColor;
    private File file;
    private String category;

    public AddController(MainPanel panel, AddGoodsFrame frame) {
        addGoodsPanel = (AddGoodsPanel) GoodController.get().getGoodsFrame().getContentPane();
        this.panel = panel;
        initListeners(frame);
    }

    private void initListeners(AddGoodsFrame frame) {
        addImageButtonListener();
        addColorButtonListener();
        addOkButtonListener(frame);
        addCancelButtonListener(frame);
    }

    private void addColorButtonListener() {
        addGoodsPanel.getSetColorButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chosenColor = JColorChooser.showDialog(addGoodsPanel, "Select Color", Color.WHITE);
                addGoodsPanel.setupColor(chosenColor);
            }
        });
    }

    private void addImageButtonListener() {
        addGoodsPanel.getSetImageButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser imageChooser = new JFileChooser();
                int confirmation = imageChooser.showDialog(addGoodsPanel, "Open image");

                if (confirmation == JFileChooser.APPROVE_OPTION) {
                    file = imageChooser.getSelectedFile();
                    addGoodsPanel.getImageField().setText(file.toString());
                }
            }
        });
    }

    private void addCancelButtonListener(AddGoodsFrame frame) {
        addGoodsPanel.getCancelButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
    }

    private void addOkButtonListener(AddGoodsFrame frame) {
        addGoodsPanel.getOkButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                category = (String) addGoodsPanel.getCategoryComboBox().getSelectedItem();
                float price = getValue(addGoodsPanel.getPriceField());
                double quantity = getValue(addGoodsPanel.getQuantityField());

                Good newGood = new Good(getImageIcon(), getName(), category, price, chosenColor, quantity, getAvailable());
                goodTableModel = (GoodTableModel) panel.getTable().getModel();
                goodTableModel.addRow(newGood);
                chosenColor = null;
                frame.dispose();
            }

            private String getName() {
                return addGoodsPanel.getNameField().getText();
            }

            private Boolean getAvailable() {
                return addGoodsPanel.getAvailableCheckBox().isSelected();
            }

            private ImageIcon getImageIcon() {
                ImageIcon image = null;
                try {
                    image = new ImageIcon(file.toString());
                } catch (Exception ignored) {
                    if (file != null) {
                        System.out.println("You input invalid value, can't open image");
                    }
                }
                return image;
            }

            private Float getValue(JTextField textField) {
                float value = 0;
                String textValue = textField.getText();
                try {
                    value = Float.parseFloat(textValue);
                } catch (Exception ignored) {
                    if (!textValue.isEmpty()) {
                        System.out.println("You input invalid value, value ignored");
                    }
                }
                return value;
            }
        });
    }
}
