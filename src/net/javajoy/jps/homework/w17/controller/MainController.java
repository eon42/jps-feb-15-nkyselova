package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.model.GoodTableModel;
import net.javajoy.jps.homework.w17.view.MainFrame;
import net.javajoy.jps.homework.w17.view.MainPanel;

/**
 * @author Kyselova Nataliia
 */
public class MainController {

    public MainController() {
        MainFrame frame = new MainFrame();
        GoodTableModel goodTableModel = ((MainPanel) frame.getContentPane()).getGoodTableModel();

        frame.useAddGoodListener(GoodController.get().createAddGoodListener());
        frame.useDeleteGoodListener((GoodController.get().createDeleteGoodListener()));
        frame.useSaveController(new SaveController(frame));
        frame.useOpenController(new OpenController(frame, goodTableModel));

        GoodController.get().setFrame(frame);
    }

}
