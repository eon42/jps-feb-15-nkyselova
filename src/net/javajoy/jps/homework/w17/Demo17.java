package net.javajoy.jps.homework.w17;

import net.javajoy.jps.homework.w17.controller.MainController;

/**
 * @author Kyselova Nataliia
 */
public class Demo17 {
    public static void main(String[] args) {
        new MainController();
    }
}
