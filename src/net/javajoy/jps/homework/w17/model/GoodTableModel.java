package net.javajoy.jps.homework.w17.model;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kyselova Nataliia
 */
public class GoodTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;

    public static final int QUANTITY_OF_COLUMN = 7;
    public static final int AVAILABLE_COLUMN = 6;
    public static final int COLOR_COLUMN = 4;
    public static final int IMG_COLUMN = 0;

    private static GoodTableModel instance;

    private List<String> nameColumn;
    private List<Good> list;

    private GoodTableModel() {
        this.list = new ArrayList<>();
        initNameColumn();
    }

    public static GoodTableModel getInstance() {
        if (instance == null) {
            instance = new GoodTableModel();
        }
        return instance;
    }

    public static void setInstance(GoodTableModel table) {
        instance = table;
    }

    private void initNameColumn() {
        nameColumn = new ArrayList<>(QUANTITY_OF_COLUMN);
        nameColumn.add("Image");
        nameColumn.add("Name");
        nameColumn.add("Category");
        nameColumn.add("Price");
        nameColumn.add("Color");
        nameColumn.add("Quantity");
        nameColumn.add("Available");
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return QUANTITY_OF_COLUMN;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object[] good = list.get(row).toArray();
        return good[column];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void addRow(Good newGood) {
        list.add(newGood);
        fireTableRowsInserted(list.size() - 1, list.size() - 1);
    }

    public void removeRow(int row) {
        if (row >= 0 && row <= list.size() - 1) {
            list.remove(row);
            fireTableRowsInserted(list.size() - 1, list.size() - 1);
            fireTableDataChanged();
        }
    }

    public void setData(List<Good> list) {
        this.list = list;
        fireTableChanged(new TableModelEvent(this));
    }

    @Override
    public String getColumnName(int index) {
        String[] nameColumnString = new String[QUANTITY_OF_COLUMN];
        for (int i = 0; i < QUANTITY_OF_COLUMN; i++) {
            nameColumnString[i] = nameColumn.get(i);
        }
        return nameColumnString[index];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == AVAILABLE_COLUMN)
            return Boolean.class;

        return super.getColumnClass(columnIndex);
    }

    public List<Good> getList() {
        return list;
    }

}

