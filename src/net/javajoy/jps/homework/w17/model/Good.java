package net.javajoy.jps.homework.w17.model;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

/**
 * @author Kyselova Nataliia
 */
public class Good implements Serializable {
    private static final long serialVersionUID = 1L;

    private ImageIcon image;
    private String name;
    private String category;
    private Float price;
    private Color color;
    private Double quantity;
    private Boolean available;

    public Good(ImageIcon image, String name, String category, Float price, Color color, Double quantity, Boolean available) {
        this.image = image;
        this.name = name;
        this.category = category;
        this.price = price;
        this.color = color;
        this.quantity = quantity;
        this.available = available;
    }

    public Object[] toArray() {
        Object[] array = new Object[7];
        array[0] = this.image;
        array[1] = this.name;
        array[2] = this.category;
        array[3] = this.price;
        array[4] = this.color;
        array[5] = this.quantity;
        array[6] = this.available;
        return array;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Good{" +
                ", image=" + image +
                ", name=" + name + '\'' +
                ", category=" + category +
                ", price=" + price +
                ", color=" + color +
                ", quantity=" + quantity +
                ", available=" + available +
                '}';
    }
}
