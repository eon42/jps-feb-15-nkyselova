package net.javajoy.jps.homework.w17.view;

import net.javajoy.jps.homework.w17.model.GoodTableModel;
import net.javajoy.jps.homework.w17.view.renderer.CellRenderer;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class MainPanel extends JPanel {
    public static final int WIDTH = 600;
    public static final int HEIGHT = 300;
    private JButton addButton;
    private JButton deleteButton;
    private JButton openPriceButton;
    private JButton savePriceButton;
    private JTable table;

    public MainPanel() {
        setLayout(new GridBagLayout());
        initTable();
        initButtons();
    }

    private void initButtons() {
        addButton = new JButton();
        addButton.setText("Add");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        add(addButton, gbc);

        deleteButton = new JButton();
        deleteButton.setText("Delete");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        add(deleteButton, gbc);

        openPriceButton = new JButton();
        openPriceButton.setText("Open price");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        add(openPriceButton, gbc);

        savePriceButton = new JButton();
        savePriceButton.setText("Save price");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        add(savePriceButton, gbc);
    }

    private void initTable() {
        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 4;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);
        add(scrollPane, gbc);
        table = new JTable(GoodTableModel.getInstance());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setMinimumSize(new Dimension(WIDTH, HEIGHT));

        scrollPane.setViewportView(table);

        table.setPreferredScrollableViewportSize(new Dimension(WIDTH, HEIGHT));
        table.getColumnModel().getColumn(GoodTableModel.COLOR_COLUMN).setCellRenderer(new CellRenderer());
        table.getColumnModel().getColumn(GoodTableModel.IMG_COLUMN).setCellRenderer(new CellRenderer());
        table.setFillsViewportHeight(true);
    }

    public JTable getTable() {
        return table;
    }

    public GoodTableModel getGoodTableModel() {
        return (GoodTableModel) table.getModel();
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getOpenPriceButton() {
        return openPriceButton;
    }

    public JButton getSavePriceButton() {
        return savePriceButton;
    }
}
