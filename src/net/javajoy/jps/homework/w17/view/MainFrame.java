package net.javajoy.jps.homework.w17.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Kyselova Nataliia
 */
public class MainFrame extends JFrame {

    public MainFrame() {
        initFrame();
    }

    private void initFrame() {
        MainPanel panel = new MainPanel();
        setPreferredSize(new Dimension(800, 300));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Table");
        setResizable(false);
        setContentPane(panel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void useAddGoodListener(ActionListener actionListener) {
        ((MainPanel) getContentPane()).getAddButton().addActionListener(actionListener);
    }

    public void useDeleteGoodListener(ActionListener actionListener) {
        ((MainPanel) getContentPane()).getDeleteButton().addActionListener(actionListener);
    }

    public void useSaveController(ActionListener actionListener) {
        ((MainPanel) getContentPane()).getSavePriceButton().addActionListener(actionListener);
    }

    public void useOpenController(ActionListener actionListener) {
        ((MainPanel) getContentPane()).getOpenPriceButton().addActionListener(actionListener);
    }
}