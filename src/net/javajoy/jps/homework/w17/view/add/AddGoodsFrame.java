package net.javajoy.jps.homework.w17.view.add;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class AddGoodsFrame extends JFrame {

    public AddGoodsFrame() {
        initFrame();
    }

    private void initFrame() {
        AddGoodsPanel panel = new AddGoodsPanel();
        setPreferredSize(new Dimension(500, 300));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Add good");
        setResizable(false);
        setContentPane(panel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
