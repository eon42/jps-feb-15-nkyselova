package net.javajoy.jps.homework.w17.view.add;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class AddGoodsPanel extends JPanel {
    public static final String FOOD = "FOOD";
    public static final String BEVERAGE = "BEVERAGE";
    public static final String CLOTH = "CLOTH";
    public static final int WIDTH_PANEL = 500;
    public static final int HEIGHT_PANEL = 300;

    private JTextField imageField;
    private JTextField nameField;
    private JComboBox<String> categoryComboBox;
    private JTextField priceField;
    private JLabel colorLabel;
    private JTextField quantityField;
    private JCheckBox availableCheckBox;
    private JButton okButton;
    private JButton cancelButton;
    private JButton setImageButton;
    private JButton setColorButton;
    private String[] categories;

    public AddGoodsPanel() {
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(WIDTH_PANEL, HEIGHT_PANEL));
        setMinimumSize(new Dimension(WIDTH_PANEL, HEIGHT_PANEL));
        setCategory();
        initComponents();
    }

    public void setupColor(Color chosenColor) {
        getColorLabel().setOpaque(true);
        getColorLabel().setForeground(chosenColor);
        getColorLabel().setBackground(chosenColor);
        getColorLabel().setText(Integer.toString(chosenColor.getRGB()));
    }

    private void setCategory() {
        categories = new String[]{
                FOOD,
                BEVERAGE,
                CLOTH,
        };
    }

    private void initComponents() {
        initImageComponents();
        initNameComponents();
        initCategoryComponents();
        initPriceComponents();
        initColorComponents();
        initQuantityComponents();
        initAvailableComponents();
        initOkButtonComponents();
        initCancelButtonComponents();
    }

    private void initCancelButtonComponents() {
        cancelButton = new JButton("Cancel");
        cancelButton.setMinimumSize(new Dimension(100, 25));
        cancelButton.setPreferredSize(new Dimension(100, 25));
        add(cancelButton, createCellConstraints(1, 7, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE));
    }

    private void initOkButtonComponents() {
        okButton = new JButton("Ok");
        okButton.setMinimumSize(new Dimension(100, 25));
        okButton.setPreferredSize(new Dimension(100, 25));
        add(okButton, createCellConstraints(1, 7, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
    }

    private void initAvailableComponents() {
        JLabel availableLabel = new JLabel("Available");
        add(availableLabel, createCellConstraints(0, 6, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        availableCheckBox = new JCheckBox("");
        add(availableCheckBox, createCellConstraints(1, 6, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private void initQuantityComponents() {
        JLabel quantityLabel = new JLabel("Quantity");
        add(quantityLabel, createCellConstraints(0, 5, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        quantityField = new JTextField("");
        add(quantityField, createCellConstraints(1, 5, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private void initColorComponents() {
        JLabel colorLabel = new JLabel("Color");
        add(colorLabel, createCellConstraints(0, 4, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        this.colorLabel = new JLabel("");
        add(this.colorLabel, createCellConstraints(1, 4, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
        setColorButton = new JButton("Choose");
        setColorButton.setMinimumSize(new Dimension(50, 19));
        setColorButton.setPreferredSize(new Dimension(50, 19));
        add(setColorButton, createCellConstraints(2, 4, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private void initPriceComponents() {
        JLabel priceLabel = new JLabel("Price");
        add(priceLabel, createCellConstraints(0, 3, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        priceField = new JTextField("");
        add(priceField, createCellConstraints(1, 3, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private void initCategoryComponents() {
        JLabel categoryLabel = new JLabel("Category");
        add(categoryLabel, createCellConstraints(0, 2, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        categoryComboBox = new JComboBox<>(categories);
        add(categoryComboBox, createCellConstraints(1, 2, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private void initNameComponents() {
        JLabel nameLabel = new JLabel("Name");
        add(nameLabel, createCellConstraints(0, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        nameField = new JTextField();
        add(nameField, createCellConstraints(1, 1, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private void initImageComponents() {
        JLabel imageLabel = new JLabel("Image");
        add(imageLabel, createCellConstraints(0, 0, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE));
        imageField = new JTextField();
        add(imageField, createCellConstraints(1, 0, 5, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
        setImageButton = new JButton("Choose");
        setImageButton.setMinimumSize(new Dimension(50, 19));
        setImageButton.setPreferredSize(new Dimension(50, 19));
        add(setImageButton, createCellConstraints(2, 0, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL));
    }

    private GridBagConstraints createCellConstraints(int gridx, int gridy, double weightX, double weightY,
                                                     int anchor, int fill) {
        return new GridBagConstraints(
                gridx,
                gridy,
                1,
                1,
                weightX,
                weightY,
                anchor,
                fill,
                new Insets(6, 6, 6, 6),
                0,
                0
        );
    }

    public JTextField getImageField() {
        return imageField;
    }

    public JTextField getNameField() {
        return nameField;
    }

    public JComboBox<String> getCategoryComboBox() {
        return categoryComboBox;
    }

    public JTextField getPriceField() {
        return priceField;
    }

    public JTextField getQuantityField() {
        return quantityField;
    }

    public JCheckBox getAvailableCheckBox() {
        return availableCheckBox;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JButton getSetImageButton() {
        return setImageButton;
    }

    public JButton getSetColorButton() {
        return setColorButton;
    }

    public JLabel getColorLabel() {
        return colorLabel;
    }
}
