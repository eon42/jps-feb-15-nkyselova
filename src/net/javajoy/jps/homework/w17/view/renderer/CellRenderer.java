package net.javajoy.jps.homework.w17.view.renderer;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @author Kyselova Nataliia
 */
public class CellRenderer extends JLabel implements TableCellRenderer {

    public CellRenderer() {
        setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        if (value instanceof Color) {
            setBackground((Color) value);
        } else if (value instanceof Icon) {
            setIcon((Icon) value);
        }

        if (isSelected) {
            setBorder(BorderFactory.createLineBorder(Color.RED));
        } else {
            setBorder(BorderFactory.createLineBorder(Color.WHITE));
        }
        return this;
    }
}
