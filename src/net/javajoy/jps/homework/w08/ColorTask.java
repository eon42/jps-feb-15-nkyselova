package net.javajoy.jps.homework.w08;

import java.awt.Color;
import java.util.Scanner;

/**
 * @author Kiselova Nataliia
 */
//AN: теперь все хорошо!
public class ColorTask {
    public static final String ALL_COLORS = "all";
    public static final String EXIT_CODE = "exit";

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        ColorStorage colorStorage = new ColorStorage();
        initDefaultColors(colorStorage);
        new ColorTask().startConsole(colorStorage);
    }

    private static void initDefaultColors(ColorStorage colorStorage) throws NoSuchFieldException, IllegalAccessException {
        colorStorage.addColor("red", Color.RED);
        colorStorage.addColor("green", Color.GREEN);
        colorStorage.addColor("blue", Color.BLUE);
        colorStorage.addColor("black", Color.BLACK);
        colorStorage.addColor("crimson", new Color(255, 192, 203));
        colorStorage.addColor("amethyst", new Color(153, 102, 204));
        colorStorage.addColor("cobalt", new Color(0, 71, 171));

        //KN:
        /* На практике в субботу ты говорил, что можно, используя класс Reflection, записать
        colorStorage.addColor("red", Color.RED);
        но вместо строки "red" использовать какие-то методы класса, чтобы получить строковое представление цвета.
        Нашла только
        Color.class.getDeclaredField("red").getName()
        но не вижу в этом смысла, так как здесь тоже нужно использовать строку "red".
        Подскажи, пожалуйста, может быть есть другой способ получить строковое представление цвета?

        AN: все верно, особо смысла нет. Только сделать так

            Field field = Color.class.getField("red"); //ищем поле в классе с именем red
            Color color = (Color)field.get(null);    // получаем значение

            через строчное имя цвета можем получить ссылку на объект с RGB. Такой примем разве что можно было бы применить, чтобы искать в классе Color готовые поля и копировать их себе
            Так что можно оставить и так. Это нормально
        */

    }

    private void startConsole(ColorStorage colorStorage) {
        Scanner scanner = new Scanner(System.in).useDelimiter(System.getProperty("line.separator"));

        showWelcomeMessage();
        String inputColor = scanner.nextLine();
        while (!inputColor.equals(EXIT_CODE)) {
            if (inputColor.equals(ALL_COLORS)) {
                printAllColors(colorStorage);
            } else {
                if (colorStorage.exists(inputColor)) {
                    System.out.println(colorStorage.getColor(inputColor));
                } else {
                    System.out.println("There is no color in list. It will be added. " +
                            "Please, input its representation in RGB.");
                    int colorRed;
                    int colorGreen;
                    int colorBlue;

                    showMessageFor("Red");
                    colorRed = inputValidateColor(scanner);

                    showMessageFor("Green");
                    colorGreen = inputValidateColor(scanner);

                    showMessageFor("Blue");
                    colorBlue = inputValidateColor(scanner);

                    System.out.println("Color was successfully added in list of color.");
                    colorStorage.addColor(inputColor, colorRed, colorGreen, colorBlue);
                }
            }
            showWelcomeMessage();
            inputColor = scanner.nextLine();
        }
    }

    private void showWelcomeMessage() {
        System.out.print("Please, input color (\"exit\" to quit)\n> ");
    }

    private void showMessageFor(String color) {
        System.out.println(color + "(digit from 0 to 255) --> ");
    }

    private static void printAllColors(ColorStorage colorStorage) {
        for (String key : colorStorage.getAllColors()) {
            System.out.println(key);
        }
    }

    private static int inputValidateColor(Scanner scanner) {
        String s = scanner.nextLine();
        String pattern = "(([0,1][0-9][0-9])|(2[0-4][0-9])|(25[0-5]))";
        while (!s.matches(pattern)) {
            System.out.print("Wrong digit. Please, try again\n> ");
            s = scanner.nextLine();
        }
        return Integer.parseInt(s);
    }
}
