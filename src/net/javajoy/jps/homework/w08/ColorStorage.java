package net.javajoy.jps.homework.w08;

import java.awt.Color;
import java.util.*;
import java.util.List;

/**
 * @author Kiselova Nataliia
 */
public class ColorStorage {
    private Map<String, Color> colorMap = new TreeMap<>();

    /**
     * Present in list of colors its name and RGB representation in string.
     *
     * @return list of colors with its RGB representation
     */
    public List<String> getAllColors() {
        List<String> colors = new ArrayList<>();

        for (Map.Entry<String, Color> colorEntry : colorMap.entrySet()) {
            colors.add(getColorInString(colorEntry.getKey()));
        }

        return colors;
    }

    /**
     * Add color in list using parameters
     *
     * @param name String
     * @param r    int
     * @param g    int
     * @param b    int
     */
    public void addColor(String name, int r, int g, int b) {
        colorMap.put(name, new Color(r, g, b));
    }

    /**
     * Check weather color already in list
     *
     * @param name
     * @return boolean
     */
    public boolean exists(String name) {
        return colorMap.containsKey(name);
    }

    /**
     * Add color in list using parameters
     *
     * @param name  String
     * @param color Color
     */
    public void addColor(String name, Color color) {
        colorMap.put(name, color);
    }

    /**
     * Present in String color name and RGB representation.
     *
     * @param name String
     * @return String
     */
    public String getColor(String name) {
        return getColorInString(name);
    }

    /**
     * Present in String color name and RGB representation.
     *
     * @param name String
     * @return String
     */
    public String getColorInString(String name) {
        return name + ": R=" + colorMap.get(name).getRed()
                + ", G=" + colorMap.get(name).getGreen()
                + ", B=" + colorMap.get(name).getBlue();
    }
}
