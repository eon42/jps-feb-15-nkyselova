package net.javajoy.jps.homework.w11.controller;

import net.javajoy.jps.homework.w11.model.GameFieldGrid;
import net.javajoy.jps.homework.w11.view.MainFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Kiselova Nataliia
 */
public class GameController {
    public static final int GRID_DIMENSION = 5;

    private MainFrame mainFrame;

    public GameController() {
        GameFieldGrid gameFieldGrid = new GameFieldGrid(GRID_DIMENSION);
        mainFrame = new MainFrame(gameFieldGrid);

        initPlayPanelListener(gameFieldGrid);
        linkObserves(gameFieldGrid);
    }

    private void initPlayPanelListener(GameFieldGrid gameFieldGrid) {
        for (int i = 0; i < gameFieldGrid.getSize(); i++) {
            for (int j = 0; j < gameFieldGrid.getSize(); j++) {
                final int finalI = i;
                final int finalJ = j;
                ActionListener actionListener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        gameFieldGrid.reverse(finalI, finalJ);
                        mainFrame.revalidate();
                        if (gameFieldGrid.isOver()) {
                            JOptionPane.showMessageDialog(mainFrame.getRootPane(), "Congratulation! You win!");
                            System.exit(0);
                        }
                    }
                };
                gameFieldGrid.getFieldGrid()[i][j].addActionListener(actionListener);
            }
        }
    }

    private void linkObserves(GameFieldGrid gameFieldGrid) {
        gameFieldGrid.addObserver(mainFrame.getInfoPanel());
        gameFieldGrid.notifyObservers();
    }
}
