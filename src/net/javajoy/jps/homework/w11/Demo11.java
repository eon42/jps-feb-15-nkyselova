package net.javajoy.jps.homework.w11;

import net.javajoy.jps.homework.w11.controller.GameController;
import net.javajoy.jps.homework.w11.view.MainFrame;

import javax.swing.*;

/**
 * @author Kiselova Nataliia
 */
public class Demo11 {

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        //AN: мне пришлось поставить такой Layout, иначе кнопки были прозрачные. Не было цветов.
        //AN: думаю есть какая-то особенность при создании фона кнопок, чтобы они были видны в MacOS Native L&F.
        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        new GameController();
    }
}
