package net.javajoy.jps.homework.w11.model;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Random;

/**
 * @author Kiselova Nataliia
 */

//AN: еще раз посмотрел на этот класс. Он получился гибридный: Model+View
public class GameFieldGrid extends Observable {
    private JButton[][] field; //AN: <-- так как это GUI компоненты
    private int size;

    public GameFieldGrid(int size) {
        this.size = size;
        field = new JButton[size][size];
        this.fillRandom();
    }

    public JButton[][] getFieldGrid() {
        return field;
    }

    public GameFieldGrid fillRandom() {
        Random random = new Random(System.currentTimeMillis());

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                this.field[i][j] = new JButton();
                if (random.nextInt(2) == 0) {
                    this.field[i][j].setBackground(Color.BLUE);
                } else {
                    this.field[i][j].setBackground(Color.WHITE);
                }
            }
        }
        return this;
    }

    public int getBlueColorCount() {
        int count = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (this.field[i][j].getBackground().equals(Color.BLUE)) {
                    count++;
                }
            }
        }
        return count;
    }

    public GameFieldGrid reverse(int row, int column) {
        for (int i = 0; i < size; i++) {
            reverseButtonColor(this.field[i][column]);
            reverseButtonColor(this.field[row][i]);
        }
        reverseButtonColor(this.field[row][column]);

        setChanged();
        notifyObservers();

        return this;
    }

    private void reverseButtonColor(JButton button) {
        if (button.getBackground().equals(Color.BLUE)) {
            button.setBackground(Color.WHITE);
        } else {
            button.setBackground(Color.BLUE);
        }
    }

    public boolean isOver() {
        return (getBlueColorCount() == size * size || getBlueColorCount() == 0);
    }

    public int getSize() {
        return size;
    }

    public int getAnotherColorCount(int anotherColorCount) {
        return size * size - anotherColorCount;
    }
}
