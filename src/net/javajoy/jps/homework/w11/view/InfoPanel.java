package net.javajoy.jps.homework.w11.view;

import net.javajoy.jps.homework.w11.model.GameFieldGrid;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Kiselova Nataliia
 */

public class InfoPanel extends JPanel implements Observer {
    private int fieldSize;
    private JLabel jLabelFirstColor;
    private JLabel jLabelSecondColor;

    private GameFieldGrid gameField;

    public InfoPanel(GameFieldGrid gameField, int fieldSize) {
        this.gameField = gameField;
        this.fieldSize = fieldSize;
        initPanel();
    }

    private void initPanel() {
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(gameField.getSize() * fieldSize, fieldSize / 2));
        initInfoLabels();
    }

    private void initInfoLabels() {
        jLabelFirstColor = new JLabel();
        jLabelSecondColor = new JLabel();
        updateInfoLabels();
        add(jLabelFirstColor, createFirstConstraints());
        add(jLabelSecondColor, createSecondConstraints());
    }

    private void updateInfoLabels() {
        int blueColorCount = gameField.getBlueColorCount();
        jLabelFirstColor.setText(" Blue " + blueColorCount);
        jLabelSecondColor.setText(" White " + gameField.getAnotherColorCount(blueColorCount));
    }

    private GridBagConstraints createFirstConstraints() {
        return new GridBagConstraints(
                0,
                0,
                1,
                1,
                1,
                1,
                GridBagConstraints.WEST,
                GridBagConstraints.BOTH,
                new Insets(1, 1, 1, 1),
                0,
                0
        );
    }

    private GridBagConstraints createSecondConstraints() {
        return new GridBagConstraints(
                1,
                0,
                1,
                1,
                1,
                1,
                GridBagConstraints.WEST,
                GridBagConstraints.BOTH,
                new Insets(1, 1, 1, 1),
                0,
                0
        );
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param o   the observable object.
     * @param arg an argument passed to the <code>notifyObservers</code>
     */
    @Override
    public void update(Observable o, Object arg) {
        updateInfoLabels();
    }
}
