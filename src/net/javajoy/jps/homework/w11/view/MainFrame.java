package net.javajoy.jps.homework.w11.view;

import net.javajoy.jps.homework.w11.model.GameFieldGrid;

import javax.swing.*;
import java.awt.*;

/**
 * @author Kiselova Nataliia
 */

public class MainFrame extends JFrame {
    public static final int FIELD_SIZE_IN_PIXELS = 100;

    private JPanel contentPanel = new JPanel(new GridBagLayout());
    private PlayPanel playPanel;
    private InfoPanel infoPanel;

    public MainFrame(GameFieldGrid gameField) throws HeadlessException {
        playPanel = new PlayPanel(gameField);
        infoPanel = new InfoPanel(gameField, FIELD_SIZE_IN_PIXELS);

        initMainPanel(gameField.getSize());
        initFrame();
        setVisible(true);
    }

    private void initFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Revers");
        setContentPane(contentPanel);
        pack();
        setLocationRelativeTo(null);
    }

    private void initMainPanel(int gridDimension) {
        infoPanel.setPreferredSize(new Dimension(gridDimension * FIELD_SIZE_IN_PIXELS, FIELD_SIZE_IN_PIXELS / 2));
        contentPanel.add(infoPanel, createConstraintsInfoPanel());
        playPanel.setPreferredSize(new Dimension(gridDimension * FIELD_SIZE_IN_PIXELS, gridDimension * FIELD_SIZE_IN_PIXELS));
        contentPanel.add(playPanel, createConstraintsPlayPanel());
    }

    private GridBagConstraints createConstraintsInfoPanel() {
        return new GridBagConstraints(0,
                0,
                1,
                1,
                1,
                1,
                GridBagConstraints.NORTH,
                GridBagConstraints.BOTH,
                new Insets(1, 1, 1, 1),
                0,
                0);
    }

    private GridBagConstraints createConstraintsPlayPanel() {
        return new GridBagConstraints(0,
                1,
                1,
                1,
                1,
                1,
                GridBagConstraints.SOUTH,
                GridBagConstraints.BOTH,
                new Insets(1, 1, 1, 1),
                0,
                0);
    }

    public InfoPanel getInfoPanel() {
        return infoPanel;
    }

    public PlayPanel getPlayPanel() {
        return playPanel;
    }
}
