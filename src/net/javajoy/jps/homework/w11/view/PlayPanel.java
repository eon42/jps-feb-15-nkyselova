package net.javajoy.jps.homework.w11.view;

import net.javajoy.jps.homework.w11.model.GameFieldGrid;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Kiselova Nataliia
 */
public class PlayPanel extends JPanel {
    private GameFieldGrid gameField;

    public PlayPanel(GameFieldGrid gameField) {
        this.gameField = gameField;
        initField();
    }

    private void initField() {
        setLayout(new GridBagLayout());
        initCells();
    }

    private void initCells() {
        for (int i = 0; i < gameField.getSize(); i++) {
            for (int j = 0; j < gameField.getSize(); j++) {
                add(gameField.getFieldGrid()[i][j], createCellConstraints(i, j));
            }
        }
    }

    private GridBagConstraints createCellConstraints(int i, int j) {
        return new GridBagConstraints(
                j,
                i,
                1,
                1,
                1,
                1,
                GridBagConstraints.WEST,
                GridBagConstraints.BOTH,
                new Insets(1, 1, 1, 1),
                0,
                0
        );
    }
}
